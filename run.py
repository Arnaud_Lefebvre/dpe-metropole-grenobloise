"""
Executes program
"""
import os
import sqlite3
import subprocess

#

import scripts.variables
from scripts.set_start_time import set_start_time
from scripts.transform_iris_geometry import transform_iris_geometry
from scripts.planning import planning
from scripts.download import download
from scripts.load import load
from scripts.create_reference_tables import create_reference_tables
from scripts.create_iris_table import create_iris_table
from scripts.duplicate_iris_columns import duplicate_iris_columns
from scripts.create_communes_table import create_communes_table
from scripts.duplicate_communes_columns import duplicate_communes_columns
from scripts.create_dpe_table import create_dpe_table
from scripts.duplicate_dpe_columns import duplicate_dpe_columns
from scripts.add_columns import add_columns

from scripts.update_get_stats_api import update_get_stats_api
from scripts.update_get_stats_local import update_get_stats_local
from scripts.update_compare_stats import update_compare_stats
from scripts.update_do_update import update_do_update

from scripts.feed_communes_table import feed_communes_table
from scripts.feed_iris_table import feed_iris_table
from scripts.feed_dpe_table import feed_dpe_table

from scripts.transform_handler import transform_handler



set_start_time() #just sets-up start_time for console logging

# transform_iris_geometry() #long script, outputs in datasets/coordonnees_iris_transformees.csv

planning() #sets-up shared variables for executing only required scripts parts

if len(scripts.variables.task_list_files) > 0: #if any download is required
    download() #then exec download script

if len(scripts.variables.task_list_reference_tables) > 0: #if any reference table creation is required
    create_reference_tables() #then exec create_reference_table script

if len(scripts.variables.task_list_raw_tables): #if any raw_data loading is required
    load() #then exec load script


if "iris" in scripts.variables.task_list_modded_tables:
    create_iris_table()
if "communes" in scripts.variables.task_list_modded_tables:
    create_communes_table()
if "dpe" in scripts.variables.task_list_modded_tables:
    create_dpe_table()

add_columns() #checks modded tables columns existence - add if missing

if "iris" in scripts.variables.task_list_modded_tables:
    duplicate_iris_columns()
if "communes" in scripts.variables.task_list_modded_tables:
    duplicate_communes_columns()


if "iris" in scripts.variables.task_list_modded_tables:
    feed_iris_table()
if "communes" in scripts.variables.task_list_modded_tables:
    feed_communes_table()


if scripts.variables.do_update: #if table raw_dpe existed already
    update_get_stats_api()
    update_get_stats_local()
    update_compare_stats() #then trigger update scripts
if scripts.variables.update_was_made: #if an update happened
    update_do_update() #then this will update raw_dpe data and save newly added ids into shared variable: new_ids_list

"""
IF dpe tables were created just now
    OR an update was made
    (in both cases, every id to be applied transform operations on were saved into shared variable new_ids_list)
THEN do transform on every column to be transformed for each id in new_ids_list
"""
for _id in scripts.variables.new_ids_list:
    scripts.variables.new_ids_list_as_one_string += str(_id)+","
scripts.variables.new_ids_list_as_one_string = scripts.variables.new_ids_list_as_one_string[:-1]

if ("dpe" in scripts.variables.task_list_modded_tables) or (scripts.variables.update_was_made):
    feed_dpe_table(ids_list=scripts.variables.new_ids_list, limit_iris_to_metropole=True)