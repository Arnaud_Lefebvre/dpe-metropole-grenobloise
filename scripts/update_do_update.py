"""
Uses ADEME API for DPE to retrieve only data that is missing in local
If it works, variable fetched_data is a list of dicts containing data
    fetched_data = [{}]
Otherwise, full csv is downloaded and saved as datasets/dpe_data_updated.csv
"""
import csv
import datetime
import sqlite3
import time

import json
import requests

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close
from scripts.update_set_stats import update_set_stats


@splitter_decorator
def update_do_update():
    """
    Uses ADEME API for DPE to retrieve only data that is missing in local
    If it works, variable fetched_data is a list of dicts containing data
        fetched_data = [{}]
    Otherwise, full csv is downloaded and saved as datasets/dpe_data_updated.csv
    """
    # Get required variables
    conn, cur = connection_open()

    cur.execute(
        """SELECT COUNT(*) FROM dpe"""
    )
    old_count_all = cur.fetchone()[0] #lines count before update
    cur.execute(
        """SELECT id FROM dpe"""
    )
    old_ids_list = [fetched[0] for fetched in cur.fetchall()] #list of all ids before update
    new_count_all = int(scripts.variables.shared_new_total_lines) #target count
    missing_lines_count = new_count_all - old_count_all
    print("MISSING LINES COUNT --------------------------", missing_lines_count)
    missed_lines_count = 0 #to count how many lines fetched missed (in case they do!)
    success_ids_list = [] #where to append successfully fetched ids
    current_count = old_count_all #in case simple case does not work, save amount succesfully fetched so far in this variable (+starting count)
    last_successful_id = None #where to save last successfully fetched id to do tries with q parameter
    try_worked = False #simply to skip while loop in case simple try worked (without q parameter)
    tries_count = 0 #safety for not stucking program - download full csv if failure to fetch all after many tries
    fetched_data = []
    csv_downloaded = False
    new_try_worked = False
    first_try_worked = False
    second_try_worked = False
    recursive_try_worked = False
    current_date_string = datetime.datetime.now().isoformat()
    current_year_string = current_date_string[0:4]
    current_month_string = current_date_string[5:7]

    def fetch_lines(sort: str="", size: str="", q: str="", select: str=""):
        """
        Returns data fetched according to parameters
        """
        """
        params full example
        params = (
            ('page', '1'),
            ('format', 'json'),
            ('sort', '-_score,-id'),
            ('size', 5),
            ('q', 'date_reception_dpe:"2020-*"'),
            ('select', 'id')
        )
        """
        print("function fetch_lines")
        params_list = [
            ('page', '1'),
            ('format', 'json')
        ]
        params_sort = sort
        params_size = size
        params_q = q
        params_select = select
        if not(params_sort == ""):
            params_list.append(('sort', params_sort))
        if not(params_size == ""):
            params_list.append(('size', params_size))
        if not(params_q == ""):
            params_list.append(('q', params_q))
        if not(params_select == ""):
            params_list.append(('select', params_select))
        params = tuple(params_list)

        with requests.get('https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/lines',params=params) as response:
            response.raise_for_status()
            data_dicts_list = json.loads(response.text)['results']
            return data_dicts_list

    def set_status(data_dicts_list):
        """
        Sets-up variables to check status update done or still work to be done
        """
        nonlocal old_ids_list
        nonlocal missed_lines_count
        nonlocal success_ids_list
        nonlocal current_count
        nonlocal last_successful_id
        nonlocal fetched_data
        for line in data_dicts_list:
            if line['id'] in old_ids_list:
                missed_lines_count += 1
            else:
                if not(line['id'] in success_ids_list):
                    success_ids_list.append(line['id'])
                    current_count += 1
                    last_successful_id = line['id']
                    if try_worked == True: #in case first download with ids worked then this will save data in the go again
                        fetched_data.append(line)

    # First do simple case to try to avoid complicated matters - optimal performances in case of success
    if missing_lines_count <= 10000:
        print("first try")
        missing_lines_dicts_list = fetch_lines(sort="-id", size=str(missing_lines_count), select="id")
        set_status(missing_lines_dicts_list)
        if missed_lines_count == 0:
            try_worked = True
            first_try_worked = True

    # Second do other simple case with extended size - still avoiding complicated matters with q parameter
    if try_worked == False:
        # Reset appropriate variables in case they changed
        missed_lines_count = 0
        success_ids_list = []
        current_count = old_count_all
        last_successful_id = None
        missing_lines_dicts_list = fetch_lines(sort="-id", size="10000", select="id")
        set_status(missing_lines_dicts_list)
        if current_count == new_count_all:
            try_worked = True
            second_try_worked = True

    # Else do while loop - no most variables reset because use second try results
    if try_worked == False:
        last_current_size = 0
        while (not(current_count == new_count_all)) and (tries_count<24): #while counts do not match & tries count is not too many
            tries_count += 1
            current_size = 6000
            date_to_pass = str(current_year_string)+"-"+str(current_month_string)+"-*"
            # Now try fetching data with q parameter and score
            missing_lines_dicts_list = fetch_lines(sort="_score", size=str(current_size), q=f'date_reception_dpe:"{date_to_pass}"', select="id")
            set_status(missing_lines_dicts_list)
            if current_count == new_count_all:
                try_worked = True
                recursive_try_worked = True
                break
            current_month_doing = int(current_month_string) - 1
            if current_month_doing < 1:
                current_month_doing = 12
                current_year_string = str(int(current_year_string) - 1)
            current_month_doing = str(current_month_doing)
            if len(current_month_doing) == 1:
                current_month_doing = "0" + current_month_doing
            current_month_string = current_month_doing


    # If all failed - download full csv
    if try_worked == False:
        print("La mise à jour des données des DPE via l'API a échoué, le csv complet va être téléchargé.")
        print("Début du téléchargement des données des diagnostics de performance énergétique...")
        with requests.get("https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/raw") as r_dpe_data:
            r_dpe_data.raise_for_status()
            with open("./datasets/dpe_data_updated.csv", "w") as file:
                file.write(r_dpe_data.text)
        print("Les données des diagnostics de performance énergétique semblent avoir été téléchargées sans erreur.")
        csv_downloaded = True


    missed_lines_count = 0
    success_ids_list = []
    current_count = old_count_all
    last_successful_id = None
    # If any try worked - restart with full data fetching
    # BELOW IS A COPY PASTE MINUS SELECT ARGUMENT @fetch_lines()
    # First do simple case to try to avoid complicated matters - optimal performances in case of success
    if (missing_lines_count <= 10000) and (first_try_worked):
        missing_lines_dicts_list = fetch_lines(sort="-id", size=str(missing_lines_count))
        set_status(missing_lines_dicts_list)
        if missed_lines_count == 0:
            new_try_worked = True

    # Second do other simple case with extended size - still avoiding complicated matters with q parameter
    if (new_try_worked == False) and (second_try_worked):
        # Reset appropriate variables in case they changed
        missed_lines_count = 0
        success_ids_list = []
        current_count = old_count_all
        last_successful_id = None
        missing_lines_dicts_list = fetch_lines(sort="-id", size="10000")
        set_status(missing_lines_dicts_list)
        if current_count == new_count_all:
            new_try_worked = True

    # Else do while loop
    if (new_try_worked == False) and (recursive_try_worked):
        time_to_wait = 30
        tries_count = 0
        last_current_size = 0
        missed_lines_count = 0
        success_ids_list = []
        current_count = old_count_all
        current_date_string = datetime.datetime.now().isoformat()
        current_year_string = current_date_string[0:4]
        current_month_string = current_date_string[5:7]
        last_successful_id = None
        while (not(current_count == new_count_all)) and (tries_count<24): #while counts do not match & tries count is not too many
            tries_count += 1
            current_size = 6000
            date_to_pass = str(current_year_string)+"-"+str(current_month_string)+"-*"
            # Now try fetching data with q parameter and score
            missing_lines_dicts_list = fetch_lines(sort="_score", size=str(current_size), q=f'date_reception_dpe:"{date_to_pass}"')
            set_status(missing_lines_dicts_list)
            if current_count == new_count_all:
                try_worked = True
                recursive_try_worked = True
                break
            current_month_doing = int(current_month_string) - 1
            if current_month_doing < 1:
                current_month_doing = 12
                current_year_string = str(int(current_year_string) - 1)
            current_month_doing = str(current_month_doing)
            if len(current_month_doing) == 1:
                current_month_doing = "0" + current_month_doing
            current_month_string = current_month_doing
            time.sleep(time_to_wait)


    # Finally update table <raw_dpe> with new values
    # Case API
    if csv_downloaded == False:
        print("csvdled =False")
        for line in fetched_data:
            keys_as_one_string = ""
            values_as_one_string = ""
            for k,v in line.items():
                if not(k.startswith("_")):
                    keys_as_one_string += str(k)+","
                    values_as_one_string += "\""+str(v).replace("\"","")+"\","
            keys_as_one_string = keys_as_one_string[:-1] #remove extra trailing comma
            values_as_one_string = values_as_one_string[:-1] #same
            cur.execute(
                f"""INSERT INTO raw_dpe({keys_as_one_string}) VALUES ({values_as_one_string})"""
            )
    # Case csv
    else:
        with open("./datasets/dpe_data_updated.csv", "r") as updated_file:
            reader = csv.DictReader(updated_file)
            success_ids_list = []
            for line in reader:
                if not(int(line['id']) in old_ids_list):
                    success_ids_list.append(line['id'])
                    keys_as_one_string = ""
                    values_as_one_string = ""
                    for k,v in line.items():
                        keys_as_one_string += str(k)+","
                        values_as_one_string += "\""+str(v).replace("\"","")+"\","
                    keys_as_one_string = keys_as_one_string[:-1] #remove extra trailing comma
                    values_as_one_string = values_as_one_string[:-1] #same
                    cur.execute(
                        f"""INSERT INTO raw_dpe({keys_as_one_string}) VALUES ({values_as_one_string})"""
                    )
            

    # Finally share successfully fetched new ids list
    scripts.variables.new_ids_list = success_ids_list
    update_set_stats()

    connection_close()