"""
Fuses .csv files created through geocoding together
"""
import csv
import datetime
import os
import sqlite3

#

import scripts.variables


def geocode_fuse_back_csv():
    """
    Fuses .csv files created through geocoding together
    """
    print("Réunification des fichiers.csv en cours...")
    titles_written = False
    if not(scripts.variables.update_was_made):
        scripts.variables.file_name_for_geocoding = "datasets/geocoding_results.csv"
    else:
        extend = datetime.datetime.now().isoformat().replace("T","_").replace(":","-").replace(".","_")
        scripts.variables.file_name_for_geocoding = "datasets/geocoding_results"+extend+".csv"
    with open(scripts.variables.file_name_for_geocoding, 'w') as fuse_here_file:
        writer = csv.writer(fuse_here_file)
        for temporary_file in os.listdir("temp"):
            if (temporary_file.startswith("fed_")) and (not(temporary_file == "fed_full.csv")):
                with open("temp/"+temporary_file, "r") as file_to_fuse:
                    reader = csv.reader(file_to_fuse)
                    if titles_written == False:
                        writer.writerow(next(reader))
                        titles_written = True
                    else:
                        next(reader) #if titles were written already, skip titles for file
                    writer.writerows(reader)
    print("Fin de la réunification des fichiers.csv.")
    print()