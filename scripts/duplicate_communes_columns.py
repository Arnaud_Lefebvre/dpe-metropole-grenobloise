"""
Duplicates communes columns that will be transformed (so as to keep raw values untouched)
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def duplicate_communes_columns():
    conn, cur = connection_open()
    for raw_column, duplicated_column in scripts.variables.communes_columns_to_duplicate.items():
        cur.execute(f"""
            UPDATE communes
            SET {duplicated_column} = {raw_column}
        """)
    connection_close()