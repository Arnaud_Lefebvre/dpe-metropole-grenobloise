"""
Every transformation is here in a function
At the end of the script is a list of operations that should be done per table per column
"""
import re
import unicodedata

#

#


def remove_accents(text : str) -> str:
    """
    Replaces accentuated characters with their non-accentuated counterparts
    """
    try:
        text = unicode(text, 'utf-8')
    except NameError as error:
        if not str(error) == "name 'unicode' is not defined":
            raise
    text = unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode("utf-8")
    return text

def remove_specials(text : str) -> str:
    """
    Removes special characters from a string
    """
    text = text.replace("#", "")
    text = text.replace(":", "")
    text = text.replace("/", "")
    text = text.replace("+", "")
    text = text.replace("-", "")
    text = text.replace("n°", "")
    text = text.replace("°", "")
    text = text.replace("(", "")
    text = text.replace(")", "")
    text = text.replace("?", "")
    text = text.replace(".", "")
    text = text.replace(",", "")
    text = text.replace("'", "")
    return text

def remove_spaces(text: str) -> str:
    """
    Removes spaces from a string
    """
    text = text.replace(" ", "")
    return text

def remove_digits(text : str) -> str:
    """
    Removes digits from a string
    """
    text = text.replace("0", "")
    text = text.replace("1", "")
    text = text.replace("2", "")
    text = text.replace("3", "")
    text = text.replace("4", "")
    text = text.replace("5", "")
    text = text.replace("6", "")
    text = text.replace("7", "")
    text = text.replace("8", "")
    text = text.replace("9", "")
    return text

def lowercase_string(text : str) -> str:
    """
    Makes a string entirely lowercase
    """
    text = text.lower()
    return text

def abbreviate_all(text : str) -> str:
    """
    Updates a string by shortening certain phrases
    """
    text = text.replace("saint","st")
    return text

def strip_simple(text : str) -> str:
    """
    Removes trailing spaces, apostrophes, mini from a string
    """
    text = text.strip(" '-")
    return text

def remove_double_spaces(text : str) -> str:
    """
    Unduplicates spaces in a string
    """
    search_for_spaces = re.search(r" + ", text)
    while search_for_spaces:
        text = text.replace(search_for_spaces.group(), " ")
        search_for_spaces = re.search(r" + ", text)
    return text

def remove_double_apostrophes(text : str) -> str:
    """
    Removes double apostrophes from a string
    """
    text = text.replace("''", "")
    return text

def remove_backslashes(text : str) -> str:
    """
    Removes backslashes from a string
    """
    text = text.replace("\\", "")
    return text

def replace_html_apos(text : str) -> str:
    """
    Replaces html apos <&apos;> with one space
    """
    text = text.replace("&apos;", " ")
    return text

def replace_apos(text : str) -> str:
    """
    Replaces apos <'> with one space
    """
    text = text.replace("\'", " ")
    return text
    
def remove_trash_numbers_4(text : str) -> str:
    """
    Removes numbers series like <x+-x+-x+-x+>
    """
    search_numbers_4 = re.search(r"(\d+-){3}\d+", text)
    if search_numbers_4:
        text = text.replace(search_numbers_4.group(), "")
    return text

def pattern_remove_trash_cases(case):
    """
    Switch case for regex patterns
    """
    pattern_switcher = {
        0: r", \(e\d+\)",
        1: r"ref(\d+\.)+\d+ ",
        2: r"ref : \d+\.\d+\.\d+ ",
        3: r"ref \d+\.\d+\.\d+\.\d+\.\d+ ",
        4: r"ref : \d+\.\d+\.\d+\.\d+\.\d+ ",
        5: r"ref :\d+\.\d+\.\d+\.\d+\.\d+ ",
        6: r" ?n?ref : g\.\d+/\w+\.?\w*/\w+\d+ ?",
        7: r"ref : ug \d+ ",
        8: r"ref : \d+/\d+/\w\.\w+/\w+ ",
        9: r"ref\. ?: \d+\.\d+\.\d+\.\d+\.\d+ ",
        10: r"ref\. \d+\.\d+\.\d+\.\d+\.?\d*? ",
        11: r" ?ref \d+ ?/ ?\d+ ?",
        12: r"ref\. \w+\d+ ",
        13: r" ref \d+ \d+ \d+ \d+",
        14: r" ref \w+ \d+\w \d+",
        15: r"ref \d+\.\d+\.\d+\.\d+\.\d+\.\d+ ",
        16: r"t\d (\w: ?\d*?-?\d*?/?)+ ",
        17: r"t3 a:3/b:11-12-15/e:39 a 41/f:47 a 49/h:64-65 ",
        18: r"(t\d ?)?(\w: ?\d+(-\d+)*/? ?)+\w (?=\d)",
        19: r"n de lot : \d+-\w\d+-\d+-\d+ ",
        20: r"\w:\d+[-/]\d+/?\w?:?\d*/?\d* ",
        21: r" cadastre \w+\d* ?\w? ?\w* ?\w?\d*$",
        22: r"cadastre : \w* ?\w? ?\w* ?\w*\d?(\d+\w ?)*(\d* ?)+-? ",
        23: r"( -? ?ug \d+$)|(ug \d+ ?-? )"
    }
    return pattern_switcher.get(case)
    
def remove_trash_cases(text : str):
    """
    Updates string by removing every pattern from list (refered to by index via function switcher)
    """
    merged_patterns = r""
    for i in range(1000): #safer than while True - 1000 should be super overkill already
        pattern = pattern_remove_trash_cases(i)
        if pattern is None:
            break
        else:
            merged_patterns += r"("
            merged_patterns += pattern
            merged_patterns += r")"
            merged_patterns += r"|"
    merged_patterns = merged_patterns[:-1]
    search_cases = re.search(merged_patterns, text)
    if search_cases:
        text = text.replace(search_cases.group(), "")
    return text
   
def pattern_set_null_trash_cases(case):
    """
    Switch case for regex patterns
    """
    pattern_switcher = {
        0: r"^$",
        1: r"^NULL$",
        2: r"NEANT"
    }
    return pattern_switcher.get(case)
  
def set_null_trash_cases(text : str):
    """
    Updates string setting cell to null for patterns from list (refered to by index via function switcher)
    """
    merged_patterns = r""
    for i in range(1000): #safer than while True - 1000 should be super overkill already
        pattern = pattern_set_null_trash_cases(i)
        if pattern is None:
            break
        else:
            merged_patterns += r"("
            merged_patterns += pattern
            merged_patterns += r")"
            merged_patterns += r"|"
    merged_patterns = merged_patterns[:-1]
    search_cases = re.search(merged_patterns, text)
    if search_cases:
        text = "NULL"
    return text

def replace_unshifted_numbers(text : str) -> str:
    """
    Replaces "unshifted numbers" with "shifted numbers" e.g.: é to 2 ; ç to 9
    """
    search_numbers_unshift = re.search(r'''^[&é"'(\-è_çà']+$''', text)
    if search_numbers_unshift:
        text = text.replace("&", "1")
        text = text.replace("é", "2")
        text = text.replace('"', "3")
        text = text.replace("'", "4")
        text = text.replace("(", "5")
        text = text.replace("-", "6")
        text = text.replace("è", "7")
        text = text.replace("_", "8")
        text = text.replace("ç", "9")
        text = text.replace("à", "0")
    return text

def remove_trailing_decimals(text: str) -> str:
    """
    Removes trailing decimals from a string
    Cannot do multiple negative lookbehinds with built-in python re
    Probably overkill to download library simply to solve it
    Hack possible do two positive lookbehinds and cancel if they return
    """
    text_search = re.search(r"(?<!^|\D)\.\d+$", text)
    if text_search:
        text_search_check_not_start_true = re.search(r"""(?<=^)\.\d+$""", text)
        text_search_check_not_start_shifted = re.search(r"""(?<=^)\.\d+$""", text)
        if text_search_check_not_start_true or text_search_check_not_start_shifted:
            return text
        else:
            text = text.replace(text_search.group(), "")
        return text

def remove_cedex(text: str) -> str:
    """
    Removes "cedex" from string
    must lower before
    """
    text = text.replace("cedex", "")
    return text

def transforms_to_be_made(table: str, column: str, text: str) -> str:
    """
    Applies every transform operation listed
        for a column: <column>
        that is from a table: <table>
        on a text: <text>
    """
    """
    Choose what transformation to apply to which column here
    Format is like follows:
    transforms_to_be_made = {
        "table1": {
            "column1": [
                list of operations to be made (function name)
            ]
            "column2": [
                list of operations to be made (function name)
            ]
        }
        "table2": {
            "column1": [
                list of operations to be made (function name)
            ]
            "column2": [
                list of operations to be made (function name)
            ]
        }
    }
    """
    # Variable to set order of execution of different transform operations
    order_of_execution = [
        replace_unshifted_numbers,
        remove_accents,
        lowercase_string,
        remove_trash_cases, # takes text PLUS LIST /!\ #changed to apply every case : no list required
        remove_specials,
        remove_spaces,
        remove_digits,
        remove_cedex,
        abbreviate_all,
        replace_apos,
        remove_double_apostrophes,
        remove_backslashes,
        replace_html_apos,
        remove_trash_numbers_4,
        remove_double_spaces, # order matters from here
        strip_simple,
        set_null_trash_cases # takes text PLUS LIST /!\ #changed to apply every case : no list required
    ]
    # Switcher variable, container for every function required to be applied for each column on each table
    transforms_to_be_made_switcher = {
        "dpe": {
            "re_numero_rue": [
                strip_simple,
                replace_unshifted_numbers,
                remove_double_spaces,
                set_null_trash_cases,
            ],
            "re_type_voie": [
                strip_simple,
                remove_accents,
                remove_specials,
                remove_spaces,
                remove_digits,
                lowercase_string,
                remove_double_spaces,
                set_null_trash_cases
            ],
            "re_nom_rue": [
                strip_simple,
                remove_trash_cases,
                replace_apos,
                remove_double_spaces,
                remove_double_apostrophes,
                replace_html_apos,
                remove_backslashes,
                remove_trash_numbers_4,
                set_null_trash_cases,
                lowercase_string,
                remove_accents,
                remove_specials
            ],
            "re_code_insee_commune": [
                strip_simple,
                remove_trailing_decimals,
                remove_double_spaces,
                set_null_trash_cases
            ],
            "re_commune_normalized": [
                strip_simple,
                remove_accents,
                remove_specials,
                remove_spaces,
                remove_digits,
                lowercase_string,
                abbreviate_all,
                replace_apos,
                remove_double_spaces,
                set_null_trash_cases,
                remove_cedex
            ],
            "re_code_postal": [
                strip_simple,
                remove_trailing_decimals,
                remove_double_spaces,
                set_null_trash_cases
            ],
            "longitude": [
                set_null_trash_cases
            ],
            "latitude": [
                set_null_trash_cases
            ]
        },
        "iris" : {
            "re_nom_commune_normalized" : [
                strip_simple,
                remove_accents,
                remove_specials,
                remove_spaces,
                remove_digits,
                lowercase_string,
                abbreviate_all,
                replace_apos,
                remove_double_spaces,
                set_null_trash_cases,
                remove_cedex
            ]
        },
        "communes" : {
            "re_nom_normalized" : [
                strip_simple,
                remove_accents,
                remove_specials,
                remove_spaces,
                remove_digits,
                lowercase_string,
                abbreviate_all,
                replace_apos,
                remove_double_spaces,
                set_null_trash_cases,
                remove_cedex
            ]
        }
    }
    # Function core
    function_list = transforms_to_be_made_switcher.get(table).get(column)
    for function in order_of_execution:
        if function in function_list:
            text = function(str(text))
    return text