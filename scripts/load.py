"""
Loads raw data into database
"""
import json
import sqlite3

import fiona
import pandas

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close
from scripts.update_set_stats import update_set_stats
from scripts.update_get_stats_api import update_get_stats_api


@splitter_decorator
def load():
    """
    Loads raw data into database
    """
    def load_dpe():
        """
        Loads raw DPE into database
        """
        print("Début du chargement en base de données des données brutes des DPE...")
        try:
            try:
                conn, cur = connection_open()
                with open('./datasets/dpe_data.csv') as dpe_file:
                    dpe_df = pandas.read_csv(dpe_file)
                cur.execute("""DROP TABLE IF EXISTS raw_dpe""")
                dpe_df.to_sql("raw_dpe", conn)
                # Setup shared variable ids_list for later transforms use
                cur.execute("""
                    SELECT id FROM raw_dpe
                """)
                ids = [fetched[0] for fetched in cur.fetchall()]
                scripts.variables.new_ids_list = ids
                connection_close()
            except Exception:
                raise Exception("at loading")
            try:
                print("GOT HERE")
                update_get_stats_api()
                update_set_stats()
            except Exception:
                raise Exception("at API query")
            del [dpe_file, dpe_df]
            print("Fin du chargement en base de données des données brutes des DPE...")
        except Exception as error:
            error_name = str(error)
            if error_name == "at loading":
                print("Un problème est survenu durant le chargement des données DPE en base de données.")
                print(error)
            elif error_name == "at API query":
                print("Un problème est survenu durant la récupération des informations de mise à jour depuis l'API pour les DPE.")
                print(error)
                raise
            else:
                raise
        print()
    def load_communes():
        """
        Loads raw communes into database
        """
        print("Début du chargement en base de données des données brutes des communes...")
        conn, cur = connection_open()
        with open("./datasets/communes_data.json", "r") as communes_file:
            communes_data = json.load(communes_file)
            cur.execute("""DROP TABLE IF EXISTS raw_communes""")
            cur.execute(
                """CREATE TABLE IF NOT EXISTS raw_communes(
                    geojson TEXT,
                    code_insee TEXT,
                    nom TEXT,
                    trigram TEXT,
                    code_postal TEXT
                )"""
            )
            for commune_data in communes_data.get('features'):
                commune_geojson = json.dumps({
                    'type': 'Feature',
                    'geometry': commune_data.get('geometry').get('coordinates'),
                })
                commune_code_insee = commune_data.get('properties').get('code_insee')
                commune_nom = commune_data.get('properties').get('nom')
                commune_trigram = commune_data.get('properties').get('trigram')
                commune_code_postal = commune_data.get('properties').get('code_postal')
                cur.execute(
                    """INSERT INTO raw_communes
                        (geojson, code_insee, nom, trigram, code_postal)
                        VALUES (?, ?, ?, ?, ?)""",
                        (commune_geojson, commune_code_insee, commune_nom, commune_trigram, commune_code_postal)
                )
        connection_close()
        del [communes_file, communes_data, commune_data, commune_geojson, commune_code_insee, commune_nom, commune_trigram, commune_code_postal]
        print("Fin du chargement en base de données des données brutes des communes...")
        print()
    def load_iris():
        """
        Loads raw IRIS into database
        """
        print("Début du chargement en base de données des données brutes des contours IRIS...")
        conn, cur = connection_open()
        cur.execute("""DROP TABLE IF EXISTS raw_iris""")
        cur.execute(
            """CREATE TABLE IF NOT EXISTS raw_iris(
                geojson TEXT,
                code_insee TEXT,
                nom_commune TEXT,
                iris TEXT,
                code_iris TEXT,
                nom_iris TEXT,
                type_iris TEXT
            )"""
        )
        path = "./datasets/IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01/IRIS-GE/" \
            "1_DONNEES_LIVRAISON_2020-07-00352/IRIS-GE_2-0_SHP_LAMB93_D038-2020/IRIS_GE.SHP"
        iris_fiona = fiona.open(path)
        for _, iris_data in iris_fiona.items():
            iris_geojson = json.dumps(iris_data.get('geometry'))
            iris_code_insee = iris_data.get('properties').get('INSEE_COM')
            iris_nom_commune = iris_data.get('properties').get('NOM_COM')
            iris_iris = iris_data.get('properties').get('IRIS')
            iris_code_iris = iris_data.get('properties').get('CODE_IRIS')
            iris_nom_iris = iris_data.get('properties').get('NOM_IRIS')
            iris_type_iris = iris_data.get('properties').get('TYP_IRIS')
            cur.execute(
                """INSERT INTO raw_iris
                    (geojson, code_insee, nom_commune, iris, code_iris, nom_iris, type_iris)
                    VALUES (?, ?, ?, ?, ?, ?, ?)""",
                    (iris_geojson, iris_code_insee, iris_nom_commune, iris_iris, iris_code_iris, iris_nom_iris, iris_type_iris)
            )
        connection_close()
        del [path, iris_fiona, iris_data, iris_geojson, iris_code_insee, iris_nom_commune, iris_iris, iris_code_iris, iris_nom_iris, iris_type_iris]
        print("Fin du chargement en base de données des données brutes des contours IRIS...")
        print()
    def load_communes_nombres_logements():
        """
        Loads raw housing counts for communes into database
        """
        print("Début du chargement en base de données des données du nombre de logements par commune...")
        conn, cur = connection_open()
        with open('./datasets/nb_logements_par_commune.csv') as communes_nombres_logements_file:
            communes_nombres_logements_df = pandas.read_csv(communes_nombres_logements_file)
        cur.execute("""DROP TABLE IF EXISTS raw_communes_nombres_logements""")
        communes_nombres_logements_df.to_sql("raw_communes_nombres_logements", conn)
        connection_close()
        del [communes_nombres_logements_file, communes_nombres_logements_df]
        print("Fin du chargement en base de données des données du nombre de logements par commune...")
        print()
    def load_iris_nombres_logements():
        """
        Loads raw housing counts for IRIS into database
        """
        print("Début du chargement en base de données des données du nombre de logements par maille IRIS...")
        conn, cur = connection_open()
        with open('./datasets/base-ic-logement-2017.CSV') as iris_nombres_logements_file:
            iris_nombres_logements_df = pandas.read_csv(iris_nombres_logements_file, delimiter=";")
            iris_nombres_logements_df = iris_nombres_logements_df[['IRIS','P17_LOG']]
        cur.execute("""DROP TABLE IF EXISTS raw_iris_nombres_logements""")
        iris_nombres_logements_df.to_sql("raw_iris_nombres_logements", conn)
        connection_close()
        del [iris_nombres_logements_file, iris_nombres_logements_df]
        print("Fin du chargement en base de données des données du nombre de logements par maille IRIS...")

    def load_switch(tasked_table: str):
        """
        Returns function corresponding to name
        """
        switcher = {
            "raw_dpe": load_dpe,
            "raw_communes": load_communes,
            "raw_iris": load_iris,
            "raw_communes_nombres_logements": load_communes_nombres_logements,
            "raw_iris_nombres_logements": load_iris_nombres_logements
        }
        return switcher.get(tasked_table)

    # Runs every function associated to a task to load raw data into database
    for _task in scripts.variables.task_list_raw_tables:
        load_switch(_task)()