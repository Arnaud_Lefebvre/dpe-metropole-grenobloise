"""
Creates modded table DPE
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def create_dpe_table():#update: bool=False, ids_list_string: str=""):
    conn, cur = connection_open()

    # Get all original column selected for modded table for shared list
    columns_list = []
    for column in scripts.variables.modded_tables_columns_dict['dpe']:
        if not(column.startswith("re_")):
            columns_list.append(column)
    # Turn them in a string comma serparated for SQL query
    columns_list_as_one_string = ""
    for column in columns_list:
        columns_list_as_one_string += column+","
    columns_list_as_one_string = columns_list_as_one_string[:-1] #remove trailing comma

    print("Création de la table <dpe> à partir des données brutes de la table <raw_dpe>...")
    cur.execute(
        f"""CREATE TABLE IF NOT EXISTS dpe AS SELECT {columns_list_as_one_string} FROM raw_dpe"""
    )
    print("Fin de la création de la table <dpe> à partir des données brutes de la table <raw_dpe>.")
    print()

    connection_close()