"""
Plans next steps for scripts exec
Sets-up shared variables according to current repository and database states to plan functions executions

Check variables_persistent file & create if needed
Check folders & create if needed
Check database & create if needed
Check tables
Setup update var
Check data files
"""
import json
import os
import sqlite3
import subprocess

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def planning():
    """
    Sets-up shared variables according to current repository and database states to plan functions executions
    """
    # List persistent variables unset
    persistent_variables_json_dumps = scripts.variables.persistent_variables_json_dumps
    # List folders to have
    folders_list = scripts.variables.folders_list
    # List database tables to have
    raw_tables_list = scripts.variables.raw_tables_list
    reference_tables_list = scripts.variables.reference_tables_list
    modded_tables_columns_dict = scripts.variables.modded_tables_columns_dict
    modded_tables_key_list = list(modded_tables_columns_dict.keys())
    # Allows linking shared variables by variable name
    tables_variables_linker = {
        "raw_tables_list": scripts.variables.task_list_raw_tables,
        "reference_tables_list": scripts.variables.task_list_reference_tables,
        "modded_tables_key_list": scripts.variables.task_list_modded_tables
    }
    # List files(values) and their associated raw table(key)
    files_dict = scripts.variables.files_dict
    # Lists files that should be downloaded along with project (no download)
    git_files_no_download_list = scripts.variables.git_files_no_download_list

    # Check folders - create if absent
    print("Vérification de la présence des dossiers nécessaires...")
    count = 0
    for folder in folders_list:
        if not os.path.isdir(f"./{folder}"):
            print(f"Le dossier <{folder}> n'existe pas : création du dossier <{folder}>.")
            subprocess.run(["mkdir", folder])
            count += 1
    if count == 0:
        print("Les dossiers nécessaires sont présents.")
    del count
    print()

    # Check variables_persistent file - create if absent
    print("Vérification de la présence des fichiers nécessaires...")
    count = 0
    if not os.path.isfile("./records/dpe_update_stamp.txt"):
        print("Le fichier <dpe_update_stamp.txt> n'existe pas : création du fichier <dpe_update_stamp.txt>.")
        with open("./records/dpe_update_stamp.txt", "w") as dpe_records_file_out:
            dpe_records_file_out.write(persistent_variables_json_dumps)
            count += 1
    if count == 0:
        print("Les fichiers nécessaires sont présents.")
    del count
    print()

    # Check database
    print("Vérification de la présence de la base de données...")
    if os.path.isfile('dpe.db'):
        print("La base de données <dpe.db> est présente.")
    else:
        print("La base de données <dpe.db> n'est pas présente, création de la base de données <dpe.db>...")
        subprocess.run(["touch", "dpe.db"])
        scripts.variables.database_existed = False
    print()

    # Check tables
    print("Vérification de la présence des tables de données...")
    # If database does not exist, append every table
    if scripts.variables.database_existed == False:
        print("La base de données n'étant pas présente, ajout de la création de chaque table à la liste de tâches...")
        for _list in [raw_tables_list, reference_tables_list, modded_tables_key_list]:
            for k,v in locals().items():
                if (v == _list) and ("_tables_" in k):
                    name = k
            for table in _list:
                tables_variables_linker[name].append(table)
    # Else if database exists then check tables existence
    else:
        count = 0
        conn, cur = connection_open()
        for _list in [raw_tables_list, reference_tables_list, modded_tables_key_list]:
            for k,v in locals().items():
                if (v == _list) and ("_tables_" in k):
                    name = k
            for table_name in _list:
                cur.execute(f"""SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}'""")
                try:
                    if not(cur.fetchone()[0] == table_name):
                        print(f"La table <{table_name}> n'existe pas, ajout de la création de la table <{table_name}> à la liste des tâches...")
                        tables_variables_linker[name].append(table_name)
                        count += 1
                except TypeError as error:
                    error_name = str(error)
                    if not error_name == "'NoneType' object is not subscriptable":
                        raise
                    else:
                        print(f"La table <{table_name}> n'existe pas, ajout de la création de la table <{table_name}> à la liste des tâches...")
                        tables_variables_linker[name].append(table_name)
                        count += 1
        if count == 0:
            print("Toutes les tables de données sont présentes.")
        connection_close()
        del count
    print()
        
    # If raw_dpe table already exists, sets-up shared variable do_update
    if not("raw_dpe" in scripts.variables.task_list_raw_tables):
        scripts.variables.do_update = True
    
    # Check data files
    print("Vérification de la présence des fichiers de données...")
    # If no table is missing
    if len(scripts.variables.task_list_raw_tables) == 0:
        print("Toutes les tables de données brutes sont présentes, saut de l'étape de vérification des fichiers de données...")
    # Else check file for every missing table
    else:
        count = 0
        for raw_table in scripts.variables.task_list_raw_tables:
            if not(os.path.isfile("./datasets/"+files_dict[raw_table]) or os.path.isdir("./datasets/"+files_dict[raw_table])):
                print(f"Le fichier {files_dict[raw_table]} nécessaire pour la création de la table {raw_table} n'est pas présent,")
                if files_dict[raw_table] in git_files_no_download_list:
                    print()
                    print(f"Veuillez télécharger le fichier {files_dict[raw_table]} disponible sur le GitLab du projet.")
                    print()
                    print()
                    raise FileNotFoundError(f"{files_dict[raw_table]} is missing")
                else:
                    print(f"        ajout du téléchargement du fichier {files_dict[raw_table]} à la liste de tâches...")
                    scripts.variables.task_list_files.append(files_dict[raw_table])
                    count += 1
        if count == 0:
            print("Toutes les fichiers de données nécessaires sont présents.")
        del count
