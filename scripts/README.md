# scripts/

Le fichier run.py exécute les scripts comme suit :  

`set_start_time` : qui ne sert qu'à enregistrer la date de début d'exécution des scripts pour information ultérieure  
`planning` : qui vérifie l'état du dépôt, de la base de données et si une mise à jour des données des DPE a été faite ; puis enregistre ces états dans des variables pour ne déclencher que les scripts nécessaires ensuite  
`download` : qui télécharge les fichiers de données nécessaires  
`create_reference_tables` : qui crée les tables de référence souhaitées obtenues à partir du lien ADEME avec le jeu de données des DPE  
`load` : qui charge les jeux de données en base  
`create_iris_table`, `create_communes_table` et `create_dpe_table` : qui créent des tables qui serviront à transformer les données sans toucher aux données brutes  
`add_columns` : qui ajoute les colonnes nécessaires à ces tables  
`duplicate_iris_columns` et `duplicate_communes_columns` : qui remplissent les nouvelles tables avec les données brutes  
`feed_iris_table` et `feed_communes_table` : qui font les transformations dans ces nouvelles tables  
`update_get_stats_api()` (aussi exécuté dans `planning` dans le cas d'une création de la table `raw_dpe`) : qui récupère les informations de mise à jour depuis l'API des DPE  
`update_get_stats_local` : qui récupère les informations de mise à jour locales  
`update_compare_stats` : qui compare ces deux précédentes informations pour savoir si une mise à jour a eu lieu  
`update_do_update` : qui fait la mise à jour si nécessaire  
`feed_dpe_table` : qui rempli la table `dpe`

Les autres scripts

Le fichier `variables.py` : contient des variables partagées  
Le fichier `splitter_decorator` : permet de décorer une fonction pour `print` des informations durant l'exécution  
Le fichier `db_conneciton_handler` : permet de gérer la connexion à la base de données  
Le fichier `deploy` : met en place tout le nécessaire pour le fichier `deploiement.ipynb`(root) pour le déploiement de la page Web  
Le fichier `update_set_stats` : écrit les informations de mise à jour en local  
Le fichier `temp_index` : sert à gérer la création et supression d'index temporaires, utiles pour augmenter les performances  
Le fichier `transform_iris_geometry` : sert à transformer les valeurs LAMBERT93 des IRIS en wgs84  

Les fichiers `duplicate_dpe_columns`, `transform_handler`, `geocode_main_py` et `place_dpe_in_iris` : sont exécutés avec `feed_dpe_table` et gérent le remplissage de la table `dpe`  
Les fichiers `transform_store`, `geocode_export_adress`, `geocode_fetch_data`, `geocode_fuse_back_csv`, `geocode_send_results_to_db` : sont utilisés par les scripts précédent  
&nbsp;&nbsp;&nbsp;&nbsp;`duplicate_dpe_columns` : remplit la table `dpe` avec les données brutes  
&nbsp;&nbsp;&nbsp;&nbsp;`transform_handler` : déclenche les transformations  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`transform_store` : contient les transformations à faire  
&nbsp;&nbsp;&nbsp;&nbsp;`geocode_main_py` : regroupe les scripts pour le géocodage  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`geocode_export_adress` : exporte les données DPE sur les adresses en multiples fichiers.csv  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`geocode_fetch_data` : requête l'API adresse.data.gouv.fr pour obtenir le géocodage  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`geocode_fuse_back_csv` : réunit les résultats obtenus en un seul fichier.csv  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`geocode_send_results_to_db` : envoie ces résultats en base de données dans la table `dpe`  
&nbsp;&nbsp;&nbsp;&nbsp;`place_dpe_in_iris` : utilise la géométrie pour postionner les coordonnées des DPE dans les mailles IRIS et sauvegarde pour chaque DPE sa maille IRIS correspondante (avant et après géocodage)