"""
Duplicates DPE columns that will be transformed (so as to keep raw values untouched)
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close
from scripts.temp_index import create_temp_index, delete_temp_index


@splitter_decorator
def duplicate_dpe_columns(ids_list_string: str):
    conn, cur = connection_open()
    # Get all original column selected for modded table for shared list
    columns_list = []
    for column in scripts.variables.modded_tables_columns_dict['dpe']:
        if not(column.startswith("re_")):
            columns_list.append(column)
    # Turn them in a string comma serparated for SQL query
    columns_list_as_one_string = ""
    for column in columns_list:
        columns_list_as_one_string += column+","
    columns_list_as_one_string = columns_list_as_one_string[:-1] #remove trailing comma
    create_temp_index("raw_dpe", ["id"])
    cur.execute("""
        SELECT COUNT(*) FROM raw_dpe
    """)
    cur.execute("""
        SELECT COUNT(*) FROM dpe
    """)
    if scripts.variables.update_was_made:
        cur.execute(f"""
            INSERT INTO dpe({columns_list_as_one_string})
            SELECT {columns_list_as_one_string}
                FROM raw_dpe
                WHERE id IN ({ids_list_string})
        """)
    cur.execute("""
        SELECT COUNT(*) FROM raw_dpe
    """)
    cur.execute("""
        SELECT COUNT(*) FROM dpe
    """)
    for raw_column, duplicated_column in scripts.variables.dpe_columns_to_duplicate.items():
        cur.execute(f"""
            UPDATE dpe
            SET {duplicated_column} = {raw_column}
            WHERE id IN ({ids_list_string})
        """)
    cur.execute("""
        SELECT COUNT(*) FROM raw_dpe
    """)
    cur.execute("""
        SELECT COUNT(*) FROM dpe
    """)
    delete_temp_index()
    connection_close()