"""
Container for variables to use throughout scripts
    & for holding variables that one might want to change in one place
"""
import json

#

#


"""
Fixed variables - all in one place
"""
# List persistent variables unset
persistent_variables_json_dumps = json.dumps({
            "last_update_date": None,
            "last_data_date": None,
            "total_lines": None,
            "total_unique_id_values": None,
            "total_unique_numero_dpe_values": None,
        })
# List folders to have
folders_list = ["datasets", "records", "graphs"]
# List database tables to have
raw_tables_list = ["raw_dpe", "raw_communes", "raw_iris", "raw_communes_nombres_logements", "raw_iris_nombres_logements"]
reference_tables_list = ["tr002_type_batiment", "tr001_modele_dpe"]
dpe_columns_to_duplicate = {
    "numero_rue": "re_numero_rue",
    "type_voie": "re_type_voie",
    "nom_rue": "re_nom_rue",
    "code_insee_commune": "re_code_insee_commune",
    "commune": "re_commune_normalized",
    "code_postal": "re_code_postal"
}
communes_columns_to_duplicate = {
    "nom": "re_nom_normalized"
}
iris_columns_to_duplicate = {
    "nom_commune": "re_nom_commune_normalized"
}
modded_tables_columns_dict = {
    "dpe": [
        "id",
        "consommation_energie",
        "classe_consommation_energie",
        "estimation_ges",
        "classe_estimation_ges",
        "annee_construction",
        "tr001_modele_dpe_id",
        "tr002_type_batiment_id",
        "secteur_activite",
        "surface_habitable",
        "date_reception_dpe",
        "code_insee_commune",
        "re_code_insee_commune",
        "code_postal",
        "re_code_postal",
        "re_code_iris",
        "re_code_iris_raw_coords",
        "numero_rue",
        "re_numero_rue",
        "type_voie",
        "re_type_voie",
        "nom_rue",
        "re_nom_rue",
        "commune",
        "re_commune_normalized",
        "longitude",
        "latitude",
        "re_geo_longitude",
        "re_geo_latitude",
        "re_geo_score",
        "re_geo_numero_rue",
        "re_geo_type_voie",
        "re_geo_nom_rue",
        "re_geo_code_postal",
        "re_geo_commune",
        "re_geo_code_insee_commune",
        "re_geo_context"
    ],
    "communes": [
        "geojson",
        "code_insee",
        "nom",
        "trigram",
        "code_postal",
        "nombre_logements",
        "re_nom_normalized"
    ],
    "iris": [
        "geojson",
        "code_insee",
        "nom_commune",
        "iris",
        "code_iris",
        "nom_iris",
        "type_iris",
        "re_wgs_geojson",
        "nombre_logements",
        "re_nom_commune_normalized"
    ]
}
# List data files(values) and their associated raw table(key)
files_dict = {
    "raw_dpe": "dpe_data.csv",
    "raw_communes": "communes_data.json",
    "raw_iris": "IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01",
    "raw_communes_nombres_logements": "nb_logements_par_commune.csv",
    "raw_iris_nombres_logements": "base-ic-logement-2017.CSV",
}
# Lists files that should be downloaded along with project (no download)
git_files_no_download_list = ["nb_logements_par_commune.csv", "coordonnees_iris_transformees.geojson"]

"""
Variables for sharing through scripts
"""
# Simple start_time variable
start_time = None

# Lists of tasks
task_list_files = []
task_list_raw_tables = []
task_list_reference_tables = []
task_list_modded_tables = []

# Boolean database existed
database_existed = True

# Boolean should update DPE
do_update = False
# Boolean update occured
update_was_made = False
# List of new ids in case an update occured
new_ids_list = []
new_ids_list_as_one_string = ""

# Update stats variables
shared_new_last_update_date = None
shared_new_last_data_date = None
shared_new_total_lines = None
shared_new_total_unique_id_values = None
shared_new_total_unique_numero_dpe_values = None

shared_old_last_update_date = None
shared_old_last_data_date = None
shared_old_total_lines = None
shared_old_total_unique_id_values = None
shared_old_total_unique_numero_dpe_values = None

# Geocoding worked boolean to allow or disallow folder cleansing
geocoding_worked_bool = True

# File name for geocoding
file_name_for_geocoding = "datasets/geocoding_results.csv"

# table_raw_emprises_exists = True #unused so far - point would be to replace markers with small polygons grouping DPEs by building