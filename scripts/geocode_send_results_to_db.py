"""
Sends results obtained through geocoding to database
"""
import csv
import os
import sqlite3
import time

#

from scripts.db_connection_handler import connection_open, connection_close
from scripts.temp_index import create_temp_index, delete_temp_index
import scripts.variables


def geocode_send_results_to_db(ids_list: list):
    """
    Sends results obtained through geocoding to database
    """
    conn, cur = connection_open()

    none_lines_count = 0
    # Check columns and create
    # columns list
    columns_list_table = [
        "re_geo_longitude",
        "re_geo_latitude",
        "re_geo_score",
        "re_geo_numero_rue",
        "re_geo_type_voie",
        "re_geo_nom_rue",
        "re_geo_code_postal",
        "re_geo_commune",
        "re_geo_code_insee_commune",
        "re_geo_context",
    ]
    columns_list_api_sent = [
        "re_numero_rue",
        "re_type_voie",
        "re_nom_rue",
        "re_code_postal",
        "commune",
        "re_code_insee_commune"
    ]
    columns_list_api_got = [
        "longitude",
        "latitude",
        "result_score",
        "result_housenumber",
        "result_street",
        "result_name",
        "result_postcode",
        "result_city",
        "result_citycode",
        "result_context"
    ]

    count = 0

    create_temp_index("dpe", columns_list_api_sent)
    create_temp_index("dpe", ["id"])

    def apply_is_null_sent(text: str) -> str:
        if text == "":
            text = "IS NULL"
        else:
            text = "= \""+text+"\""
        return text
    def apply_is_null_got(text: str) -> str:
        if text == "":
            text = "NULL"
        else:
            text = "\""+text+"\""
        return text
    print("COUNT IN SCRIPT GEOCODE_SEND_RESULTS_TO_DB LINE 78")
    print("COUNT CHECK raw_dpe")
    cur.execute("""
        SELECT COUNT(*) FROM raw_dpe
    """)
    print(cur.fetchall())
    print("COUNT CHECK dpe")
    cur.execute("""
        SELECT COUNT(*) FROM dpe
    """)
    print(cur.fetchall())
    i=0
    ii=0
    _start_time = time.time()
    with open(scripts.variables.file_name_for_geocoding, 'r') as _file:
        reader = csv.DictReader(_file)
        for line in reader:
            i+=1
            if not(i%10000):
                _now_time = round(_start_time - time.time(),2)
                print(f"geocode send results to db - - - ligne :   {i}    after {_now_time}s")
            for column_api_sent in columns_list_api_sent:
                line[column_api_sent] = apply_is_null_sent(line[column_api_sent])
            for column_api_got in columns_list_api_got:
                line[column_api_got] = apply_is_null_got(line[column_api_got])
            num = line["re_numero_rue"]
            typ = line["re_type_voie"]
            nom = line["re_nom_rue"]
            cop = line["re_code_postal"]
            com = line["commune"]
            cic = line["re_code_insee_commune"]

            cur.execute(f"""
                SELECT id
                FROM dpe
                WHERE re_numero_rue {num}
                    AND re_type_voie {typ}
                    AND re_nom_rue {nom}
                    AND re_code_postal {cop}
                    AND commune {com}
                    AND re_code_insee_commune {cic}
            """)
            try:
                line_id = cur.fetchone()[0]
            except TypeError as error:
                if not(str(error)) == "'NoneType' object is not subscriptable":
                    raise
                else:
                    none_lines_count += 1
            if line_id in ids_list:
                ii+=1
                cur.execute(f"""
                    UPDATE dpe
                    SET re_geo_longitude = {line.get('longitude')},
                        re_geo_latitude = {line.get('latitude')},
                        re_geo_score = {line.get('result_score')},
                        re_geo_numero_rue = {line.get('result_housenumber')},
                        re_geo_type_voie = {line.get('result_street')},
                        re_geo_nom_rue = {line.get('result_name')},
                        re_geo_code_postal = {line.get('result_postcode')},
                        re_geo_commune = {line.get('result_city')},
                        re_geo_code_insee_commune = {line.get('result_citycode')},
                        re_geo_context = {line.get('result_context')}
                    WHERE re_numero_rue {num}
                        AND re_type_voie {typ}
                        AND re_nom_rue {nom}
                        AND re_code_postal {cop}
                        AND commune {com}
                        AND re_code_insee_commune {cic}
                """)
    print(f"{ii} lignes ont été mises à jour")
    print(f"{none_lines_count} LIGNES ETAIENT NULL/NONE")
    delete_temp_index()

    connection_close()