"""
Handles connection to database to prevent "database is locked" error
"""
import os
import sqlite3

#

import scripts.variables


conn = None
cur = None
conn_is_open = False
count_open = 0
count_close = 0
def connection_open():
    """
    Returns connection, cursor
    """
    global conn
    global cur
    global conn_is_open
    global count_open
    if os.path.exists("dpe.db"):
        if conn_is_open == False:
            conn = sqlite3.connect("dpe.db")
            cur = conn.cursor()
            conn_is_open = True
        count_open += 1
        return conn, cur
    else:
        raise ConnectionError("Cannot connect to database that does not exist.")
def connection_close():
    """
    Closes connection previously opened with the same script's connection_open() function
    """
    global conn
    global cur
    global conn_is_open
    global count_open
    global count_close
    if (conn is not None) and (cur is not None):
        count_close += 1
        if count_open == count_close:
            conn.commit()
            cur.close()
            conn.close()
            conn = None
            cur = None
            conn_is_open = False