"""
Fetches coordinates from remade addresses
    from temporary files written via script geocode_export_addresses
    to temporary files starting with "fed_"
    into folder temp/
"""
import os
import time

import requests

import scripts.variables


def geocode_fetch_data():
    """
    Fetches coordinates from remade addresses
        from temporary files written via script geocode_export_addresses
        to temporary files starting with "fed_"
        into folder temp/
    """
    api_key_gouv = "eyJhbGciOiJIUzUxMiJ9.eyJ1c2VyIjoiNjA2ZWQyMDc0NTc5OTkyMjI4YTRmZGRhIiwidGltZSI6MTYxOTg5MTM5MS44MzM0NTM0fQ.K3bLxnJm0khAkz2xS77EOCcw6RNk4wAdfyOpFcnn0X-Lfcu07E-NOdvVx5B9N7yWzhstgLDwxUx1yv2oKDZ9ig"
    url = "https://api-adresse.data.gouv.fr/search/csv/"
    auth = requests.auth.HTTPBasicAuth("saving_dpe", api_key_gouv)
    file_i = 0
    i=0

    start_time = time.time()

    def print_time_spent(wxc=None):
        """
        Simply prints some time spent + message
        """
        now_time = round(time.time() - start_time, 2)
        if wxc is not None:
            print(str(now_time)+f"s écoulées depuis le début de la géolocalisation, début du traitement du fichier : {wxc}")
        else:
            print(str(now_time)+"s écoulées depuis le début de la géolocalisation")


    for file_to_query in os.listdir("temp"):
        if (file_to_query == "full.csv") or (file_to_query.startswith("fed_")):
            continue
        print_time_spent(file_to_query)
        with open(f"temp/{file_to_query}", "rb") as data_file:
            with open(f"temp/fed_{file_to_query}", "w") as fw:
                if auth is not None:
                    with requests.post(url, files={"data":data_file}, auth=auth) as r:
                        r.raise_for_status()
                        fw.write(r.text)
                else:
                    with requests.post(url, files={"data":data_file}) as r:
                        r.raise_for_status()
                        fw.write(r.text)
        sleep_lenght = 3
        print(f"Courte période d'attente avant la prochaine requête (pour ne pas surcharger l'API) : {sleep_lenght}s...")
        time.sleep(sleep_lenght)

    print()
    print("Vérification du nombre de lignes traitées par scission du fichier.csv")
    for temporary_file in os.listdir("temp"):
        if temporary_file.startswith("fed_"):
            temporary_file_name = temporary_file[4:-4]
            start_value, end_value = temporary_file_name.split("-")
            with open("temp/"+temporary_file, "r") as file_to_check:
                line_count = len(file_to_check.readlines())-1
                if not(line_count == (1 + int(end_value) - int(start_value))):
                    print(f"Un problème semble être survenu durant la géolocalisation (lignes : {start_value} à {end_value}.)")
                    print("Les fichiers temporaires pour la géolocalisation ne seront pas supprimés")
                    print("    (de manière à pouvoir vérifier la source/l'état du problème)")
                    scripts.variables.geocoding_worked_bool = False

    print()
    print_time_spent()
    print("Fin de la géolocalisation.")