"""
Gets saved intel from DPE API to know whether an update occured
"""
import json

#

import scripts.variables


def update_get_stats_local():
    """
    Gets saved intel from DPE API to know whether an update occured
    """
    with open("./records/dpe_update_stamp.txt", "r") as stamp_file:
        records_dict = json.loads(stamp_file.read())
        scripts.variables.shared_old_last_update_date = records_dict.get("last_update_date")
        scripts.variables.shared_old_last_data_date = records_dict.get("last_data_date")
        scripts.variables.shared_old_total_lines = records_dict.get("total_lines")
        scripts.variables.shared_old_total_unique_id_values = records_dict.get("total_unique_id_values")
        scripts.variables.shared_old_total_unique_numero_dpe_values = records_dict.get("total_unique_numero_dpe_values")