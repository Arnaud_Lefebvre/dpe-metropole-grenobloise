"""
Places DPE in IRIS
Takes DPE's list of ids to be updated
"""
import json
import sqlite3

import shapely
from shapely import geometry

from scripts.splitter_decorator import splitter_decorator
from scripts.db_connection_handler import connection_open, connection_close
from scripts.temp_index import create_temp_index, delete_temp_index


@splitter_decorator
def place_dpe_in_iris(ids_list: list, limit_iris_to_metropole: bool):
    """
    Places DPE in IRIS
    Takes DPE's list of ids to be updated
    """
    conn, cur = connection_open()
    def do_iris_polygon_from_geojson(geojson: str):
        """
        Returns a shapely Polygon from IRIS geojson
        """
        geojson_loaded = json.loads(geojson)
        polygon = shapely.geometry.Polygon(geojson_loaded['coordinates'][0])
        return polygon
    def do_point_from_coords(lon: str, lat: str):
        """
        Returns a shapely Point from a set of coordinates
        """
        try:
            lon = float(lon)
            lat = float(lat)
            point = shapely.geometry.Point(lon, lat)
        except TypeError as error:
            error_name = str(error)
            if not(error_name) == "float() argument must be a string or a number, not 'NoneType'":
                raise
            else:
                point = None
        return point
    def feed_results_to_table(feed_re_coords: bool, feed_id: str, feed_code_iris: str):
        """
        Updates DPE table
        feed_re_coords: bool
            ON: updates re_code_iris
            OFF: updates re_code_iris_raw_coords
        feed_id: str
            id of the line to be updated
        feed_code_iris: str
            code_iris to be added to the column
        """
        nonlocal conn
        nonlocal cur
        column_name = ""
        if feed_re_coords:
            column_name = "re_code_iris"
        else:
            column_name = "re_code_iris_raw_coords"
        feed_id = "\""+feed_id+"\""
        feed_code_iris = "\""+feed_code_iris+"\""
        cur.execute(f"""
            UPDATE dpe
            SET {column_name} = {feed_code_iris}
            WHERE id = {feed_id}
        """)


    # Do exception: varcesallieresetrisset
    #   codes IRIS 385240102 périphérie
    #           &  385240101 centre     <-- centre first
    # Fetch data on iris
    cur.execute("""
        SELECT code_iris, re_wgs_geojson
        FROM iris
        WHERE re_wgs_geojson IS NOT NULL
    """)
    iris_data = [{
        "code_iris": fetched[0],
        "geojson": fetched[1],
        "polygon": do_iris_polygon_from_geojson(fetched[1])
        }
        for fetched in cur.fetchall()
    ]
    # Exception data on IRIS
    #   Exception large IRIS code that "steals" from smaller IRIS that it contains
    exception_iris_code = "385240102"
    #   Exception smaller IRIS data that can be "stolen" from
    exception_iris_row = {}
    for iris_row in iris_data:
        if iris_row["code_iris"] == "385240101": #exception = smaller IRIS (that is inside bigger IRIS)
            exception_iris_row["code_iris"] = iris_row["code_iris"]
            exception_iris_row["geojson"] = iris_row["geojson"]
            exception_iris_row["polygon"] = iris_row["polygon"]
    # Fetch geo data on DPE
    if limit_iris_to_metropole:
        cur.execute("""
            SELECT id, longitude, latitude, re_geo_longitude, re_geo_latitude
            FROM dpe
                JOIN communes
                    ON communes.code_insee=dpe.re_code_insee_commune
        """)
    else:
        cur.execute("""
            SELECT id, longitude, latitude, re_geo_longitude, re_geo_latitude
            FROM dpe
        """)
    dpe_data = [{
            "id": fetched[0],
            "longitude": fetched[1],
            "latitude": fetched[2],
            "re_geo_longitude": fetched[3],
            "re_geo_latitude": fetched[4]
        }
        for fetched in cur.fetchall()
    ]

    create_temp_index("dpe", ["id"])
    i=0
    print(f"{len(dpe_data)} lignes à traiter.")
    for dpe_row in dpe_data:
        i+=1
        if not(i % 10000):
            print(f"{i} lignes traitées.")
        if dpe_row['id'] in ids_list:
            point_done = False
            re_point_done = False
            point = do_point_from_coords(dpe_row['longitude'], dpe_row['latitude'])
            re_point = do_point_from_coords(dpe_row['re_geo_longitude'], dpe_row['re_geo_latitude'])
            if point is not None:
                for iris_row in iris_data:
                    if iris_row['code_iris'] == exception_iris_code: #check whether IRIS is the exception
                        if exception_iris_row['polygon'].contains(point): #if it is the exception, do actions specific to exception
                            cur.execute(f"""
                                UPDATE dpe
                                SET re_code_iris_raw_coords = {exception_iris_row['code_iris']}
                                WHERE id = {dpe_row['id']}
                            """)
                            # save_results(_id=str(dpe_row['id']), _re_coords=False, _result=exception_iris_row['code_iris'])
                            point_done = True #prevent erasing exception handling with this switcher used next
                    # After exception check (& switcher set), do anyways
                    if (iris_row['polygon'].contains(point)) and (not(point_done)):
                        # feed_results_to_table(feed_re_coords=False, feed_id=str(dpe_row['id']), feed_code_iris=iris_row['code_iris'])
                        cur.execute(f"""
                            UPDATE dpe
                            SET re_code_iris_raw_coords = {iris_row['code_iris']}
                            WHERE id = {dpe_row['id']}
                        """)
                        # save_results(_id=str(dpe_row['id']), _re_coords=False, _result=iris_row['code_iris'])
                        point_done = True
                    if point_done: #if finding points is complete
                        break #go on looping with next point (hence exit looping on polygons for completed points)
            if re_point is not None:
                for iris_row in iris_data:
                    if iris_row['code_iris'] == exception_iris_code: #check whether IRIS is the exception
                        if exception_iris_row['polygon'].contains(re_point): #same exception case as above
                            # feed_results_to_table(feed_re_coords=True, feed_id=str(dpe_row['id']), feed_code_iris=exception_iris_row['code_iris'])
                            cur.execute(f"""
                                UPDATE dpe
                                SET re_code_iris = {exception_iris_row['code_iris']}
                                WHERE id = {dpe_row['id']}
                            """)
                            # save_results(_id=str(dpe_row['id']), _re_coords=True, _result=exception_iris_row['code_iris'])
                            re_point_done = True
                    # After exception check (& switcher set), do anyways
                    if (iris_row['polygon'].contains(re_point)) and (not(re_point_done)):
                        # feed_results_to_table(feed_re_coords=True, feed_id=str(dpe_row['id']), feed_code_iris=iris_row['code_iris'])
                        cur.execute(f"""
                            UPDATE dpe
                            SET re_code_iris = {iris_row['code_iris']}
                            WHERE id = {dpe_row['id']}
                        """)
                        # save_results(_id=str(dpe_row['id']), _re_coords=True, _result=iris_row['code_iris'])
                        re_point_done = True
                    if re_point_done: #if finding points is complete
                        break #go on looping with next point (hence exit looping on polygons for completed points)

    delete_temp_index()

    connection_close()