"""
Replaces update stamp values
"""
import json

#

import scripts.variables


def update_set_stats():
    """
    Replaces update stamp values
    """ 
    print(scripts.variables.shared_new_last_update_date)
    print(scripts.variables.shared_new_last_data_date)
    print(scripts.variables.shared_new_total_lines)
    print(scripts.variables.shared_new_total_unique_id_values)
    print(scripts.variables.shared_new_total_unique_numero_dpe_values)
    with open("./records/dpe_update_stamp.txt", "w") as stamp_file_to_write:
        stamp_file_to_write.write(json.dumps({
            "last_update_date": scripts.variables.shared_new_last_update_date,
            "last_data_date": scripts.variables.shared_new_last_data_date,
            "total_lines": scripts.variables.shared_new_total_lines,
            "total_unique_id_values": scripts.variables.shared_new_total_unique_id_values,
            "total_unique_numero_dpe_values": scripts.variables.shared_new_total_unique_numero_dpe_values
        }))