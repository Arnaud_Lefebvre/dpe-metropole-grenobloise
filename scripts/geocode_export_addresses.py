"""
Export re_addresses from table dpe to prepare for geocoding
"""
import csv
import sqlite3
import subprocess

#

from scripts.db_connection_handler import connection_open, connection_close


def geocode_export_address(ids_list: list):
    """
    Export re_addresses from table dpe to prepare for geocoding
    """
    print("Début de l'export en .csv des données transformées sur les adresses pour géolocalisation...")
    subprocess.run(["mkdir", "temp"])

    conn, cur = connection_open()

    ids_list_as_one_string = ""
    for _id in ids_list:
        ids_list_as_one_string += str(_id)+","
    ids_list_as_one_string = ids_list_as_one_string[:-1]

    cur.execute(f"""
        SELECT re_numero_rue, re_type_voie, re_nom_rue, re_code_postal, commune, re_code_insee_commune
        FROM dpe
        WHERE NOT (
            (re_numero_rue IS NULL)
            AND (re_type_voie IS NULL)
            AND (re_nom_rue IS NULL)
        )
            AND id IN ({ids_list_as_one_string})
        GROUP BY re_numero_rue, re_type_voie, re_nom_rue, re_code_postal, commune, re_code_insee_commune
    """)

    with open('temp/full.csv', 'w') as export_file:
        writer = csv.writer(export_file)
        writer.writerow(["re_numero_rue", "re_type_voie", "re_nom_rue", "re_code_postal", "commune", "re_code_insee_commune"])
        writer.writerows([a for a in cur.fetchall()])

    with open('temp/full.csv') as countit:
        line_count = len(countit.readlines())-1
    print("Les données ont été exportées.")
    print()
    print(f"Début de la scission du fichier pour requêtes API séparées, le fichier comprend {line_count} lignes...")

    file_number_start = 1
    file_number_end = 5000

    with open('temp/full.csv', 'r') as readit:
        if file_number_end > line_count:
            file_number_end = line_count
        readit_reader = csv.reader(readit)
        titles = next(readit_reader) #skip titles and save them to variable
        writeit = open(f'temp/{file_number_start}-{file_number_end}.csv', 'w')
        writeit_writer = csv.writer(writeit)
        writeit_writer.writerow(titles)
        for i,row in enumerate(readit_reader):
            if (not i % 5000 ) and (not i == 0):
                print(f"{file_number_end} lignes traitées")
                file_number_start += 5000
                file_number_end += 5000
                if file_number_end > line_count:
                    file_number_end = line_count
                writeit.close()
                writeit = open(f'temp/{file_number_start}-{file_number_end}.csv', 'w')
                writeit_writer = csv.writer(writeit)
                writeit_writer.writerow(titles)
            writeit_writer.writerow(row)
        print(f"{file_number_end} lignes traitées")
    writeit.close()

    connection_close()
    
    print("Fin de la scission en multiples .csv des données transformées sur les adresses pour géolocalisation...")
    print()