"""
Creates modded table IRIS
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def create_iris_table():
    conn, cur = connection_open()

    print("Création de la table <iris> à partir des données brutes de la table <raw_iris>...")
    cur.execute("""CREATE TABLE IF NOT EXISTS iris AS SELECT * FROM raw_iris""")
    print("Fin de la création de la table <iris> à partir des données brutes de la table <raw_iris>.")
    print()

    connection_close()