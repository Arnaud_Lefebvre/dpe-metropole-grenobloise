"""
Handles scripts for geocoding
"""
import os
import subprocess

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.geocode_export_addresses import geocode_export_address
from scripts.geocode_fetch_data import geocode_fetch_data
from scripts.geocode_fuse_back_csv import geocode_fuse_back_csv
from scripts.geocode_send_results_to_db import geocode_send_results_to_db


@splitter_decorator
def geocode_main_py(ids_list: list):
    """
    Handles scripts for geocoding
    """
    if not(os.path.exists("datasets/geocoding_results.csv")) or (scripts.variables.update_was_made):
        geocode_export_address(ids_list=ids_list)
        geocode_fetch_data()
        geocode_fuse_back_csv()
    geocode_send_results_to_db(ids_list=ids_list)