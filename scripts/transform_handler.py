"""
Handles connexion and sqlite3 script for transforming data
"""
import datetime
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
from scripts.db_connection_handler import connection_open, connection_close
from scripts.transform_store import transforms_to_be_made
from scripts.temp_index import create_temp_index, delete_temp_index


@splitter_decorator
def transform_handler(table: str, ids_list: list=[]):
    """
    Applies transform operations for every column listed in this script (transform_handler)
    <table> to be transformed
    """
    # List of columns for tables to be transformed
    columns_list_tables_dict = {
        "dpe": [
            "re_numero_rue",
            "re_type_voie",
            "re_nom_rue",
            "re_code_insee_commune",
            "re_commune_normalized",
            "re_code_postal",
            "longitude",
            "latitude"
        ],
        "iris": [
            "re_nom_commune_normalized"
        ],
        "communes": [
            "re_nom_normalized"
        ]
    }
    def do_transform(table: str, column: str, ids_list=[]):
        """
        Transform a column's values for a table
        Transformations that will be applied are listed in script transform_store 
        """
        conn, cur = connection_open()
        
        if table == "dpe":
            ids_list_as_one_string = ""
            for id in ids_list:
                ids_list_as_one_string += str(id)+","
            ids_list_as_one_string = ids_list_as_one_string[:-1]

        if table == "dpe":
            create_temp_index(table, ["id", column])
        else:
            create_temp_index(table, [column])

        if table == "dpe":
            cur.execute(f"""SELECT DISTINCT {column} FROM {table} WHERE id IN ({ids_list_as_one_string}) ORDER BY {column}""")
        else:
            cur.execute(f"""SELECT DISTINCT {column} FROM {table} ORDER BY {column}""")
        results = [fetched[0] for fetched in cur.fetchall() if fetched[0] is not None]

        # if any(character in _results for character in string.ascii_uppercase for _results in results):
        with open("./records/transform_records.md", "a") as file_it:
            file_it.write(f"# Date : {datetime.datetime.now()}\n<br>\n<br>")
            file_it.write(f"# Transformation pour la colonne <{column}> de la table <{table}>\n<br>\n<br>")
            for _result in results:
                result = str(_result)
                new = transforms_to_be_made(table, column, result)
                if (not(new == result)) or (new=="NULL"):
                    if new == "NULL":
                        cur.execute(f'''UPDATE {table} SET {column} = NULL WHERE {column} = "{result}"''')
                        file_it.write(f"La valeur de cellule `{result}` a été passée nulle\n<br>")
                    else:
                        cur.execute(f'''UPDATE {table} SET {column} = "{new}" WHERE {column} = "{result}"''')
                        file_it.write(f"La valeur de cellule `{result}` a été transformée en la valeur : `{new}`\n<br>")
            file_it.write(f"# Fin de l'enregistrement pour la colonne <{column}> de la table <{table}>.\n<br>\n<br>")
            file_it.write(f"# Date : {datetime.datetime.now()}\n<br>\n<br>")
            file_it.write(f"# -------------------------------\n<br>\n<br>")
        # else:
        #     print(f"Il n'y a pas besoin de retravailler la colonne <{column}> pour la table <{table}>.")

        delete_temp_index()

        connection_close()

    columns_list = columns_list_tables_dict[table]
    for column in columns_list:
        if table == "dpe":
            do_transform(table, column, ids_list)
        else:
            do_transform(table, column)