"""
Transforms IRIS polygons coordinates from Lambert-93(EPSG:2154) to WGS-84(standard ; EPSG:4326)
Very long to execute, run.py does not run it
Writes output to file datasets/coordonnees_iris_transformees.csv
File available on GitLab

Note: did it with nom_commune+levenshtein but would have been better with code_insee (likely)
"""
import json
import sqlite3
import warnings

import Levenshtein
import pyproj
from pyproj import Proj, transform

from scripts.splitter_decorator import splitter_decorator
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def transform_iris_geometry():
    """
    Transforms IRIS polygons coordinates from Lambert-93(EPSG:2154) to WGS-84(standard ; EPSG:4326)
    Very long to execute, run.py does not run it
    Writes output to file datasets/coordonnees_iris_transformees.csv
    File available on GitLab
    """
    transform_count = 0
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    def project_coord(lambert_lat, lambert_lon):
        """
        Projects coordinates from Lambert 93 to EPSG 4326 (lat, lon usual system)
        """
        nonlocal transform_count
        lat, lon = transform(pyproj.CRS("EPSG:2154"), pyproj.CRS("EPSG:4326"), lambert_lat, lambert_lon)
        transform_count += 1
        if not(transform_count%100):
            print(f"{transform_count} coordonnées ont été transformées depuis le début.")
        return lat, lon
    def rewrite_geojson(lambert_geojson):
        """
        Rewrites a geojson string to geojson string with transposed coordinates from function project_coord()
        """
        lambert_geojson = lambert_geojson.replace("(","[")
        lambert_geojson = lambert_geojson.replace(")","]")
        lambert_geojson = lambert_geojson.replace("'","\"")
        geojson = json.loads(lambert_geojson)
        new_geojson = {
            'type': 'Polygon',
            'coordinates': [[]]
        }
        for coordinate in geojson.get('coordinates')[0]:
            lat, lon = project_coord(float(coordinate[0]), float(coordinate[1]))
            new_geojson['coordinates'][0].append([lon, lat])
        new_geojson = json.dumps(new_geojson)
        return new_geojson 
    def transform_iris_projections():
        """
        Updates table <iris> with a new column <geojson_lon_lat> with transposed coordinates, ready to use for visualization
        """
        conn, cur = connection_open()
        cur.execute("""SELECT code_insee FROM communes""")
        codes_insee = [fetched[0] for fetched in cur.fetchall()]
        cur.execute("""SELECT geojson, code_insee, code_iris FROM iris""")
        iris_data = [
            {
            "geojson": fetched[0],
            "code_insee": fetched[1],
            "code_iris": fetched[2]
            }
        for fetched in cur.fetchall()
        ]
        with open ("./datasets/coordonnees_iris_transformees.csv","w") as dump_file:
            dump_file.write("wgs_geojson;code_insee;code_iris\n")
            i=0
            for iris_row in iris_data:
                i+=1
                if iris_row["code_insee"] in codes_insee:
                    print(f"ligne {i}: Un code_insee a été trouvée, transformation des coordonnées en cours...")
                    rewritten_geojson = rewrite_geojson(iris_row["geojson"])
                    dump_file.write(rewritten_geojson+";"+iris_row["code_insee"]+";"+iris_row["code_iris"]+"\n")
        connection_close()
    transform_iris_projections()
    del transform_count
    warnings.filterwarnings("default", category=DeprecationWarning)