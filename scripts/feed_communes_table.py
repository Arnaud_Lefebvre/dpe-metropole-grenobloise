"""
Feeds enrichment values into modded table for communes
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
from scripts.transform_handler import transform_handler
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def feed_communes_table():
    """
    Feeds enrichment values into modded table for communes
    """
    conn, cur = connection_open()
    # Feed nombre_logements from raw_communes_nombres_logements
    cur.execute("""
        UPDATE communes
        SET nombre_logements = (
                SELECT "Logements 2017"
                FROM raw_communes_nombres_logements
                WHERE "Code"=communes.code_insee
            )
    """)
    transform_handler("communes")

    connection_close()