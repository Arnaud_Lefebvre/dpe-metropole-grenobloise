"""
Creates and deletes temporary index for fastening operations on database tables
"""
import sqlite3

#

from scripts.db_connection_handler import connection_open, connection_close


indices_list = []
def create_temp_index(table: str, columns_list: list):
    """
    Creates temporary indices for table and columns list
    """
    global indices_list
    try: #late commit does not prevent index creation in failed executions hence try/except
        conn, cur = connection_open()
        for column in columns_list:
            index = f"temp_index_{table}_{column}"
            indices_list.append(index)
            cur.execute(
                f"""CREATE INDEX IF NOT EXISTS {index} ON {table}({column})"""
            )
        if len(columns_list) > 1:
            columns_as_one_string = ""
            for column in columns_list:
                columns_as_one_string += column+"_"
            columns_as_one_string = columns_as_one_string[:-1]
            index = f"temp_index_{table}_{columns_as_one_string}"
            indices_list.append(index)
            columns_as_one_string = ""
            for column in columns_list:
                columns_as_one_string += column+","
            columns_as_one_string = columns_as_one_string[:-1] #remove extra trailing comma
            cur.execute(
                f"""CREATE INDEX IF NOT EXISTS {index} ON {table}({columns_as_one_string})"""
            )
        connection_close()
    except Exception:
        raise
def delete_temp_index():
    """
    Deletes previously created indices
    Takes no argument, indices created are saved in local variable
    """
    global indices_list
    conn, cur = connection_open()
    for index in indices_list:
        cur.execute(
            f"""DROP INDEX {index}"""
        )
    connection_close()
    indices_list = []