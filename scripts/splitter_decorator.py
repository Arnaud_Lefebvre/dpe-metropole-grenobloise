"""
Decorator function to structure console logging
"""
import time
from datetime import datetime

#

import scripts.variables


def splitter_decorator(function):
    """
    Decorator for printing separator for function execution, displaying date, time since program start and function name
    Call <function>.nodeco() to execute without decorator
    """
    # Lenght of prints
    lenght = 160
    # Sets-up message to be printed
    def print_message(name: str, end: bool=False) -> str:
        """
        Returns message to be printed

        Parameters:
            name: decorated function name
            Default (end=False): start message
            end=True: Replaces 'Start' with 'End' in message
        """
        start_time = scripts.variables.start_time
        current_time = str(round(time.time() - start_time, 2))
        message = " Start of: "+name+" on "+datetime.now().isoformat()+", "+current_time+"s after start of program "
        while len(message)<lenght:
            message = "-"+message+"-"
        if len(message)>lenght:
            message = message[1:]
        if end == True:
            message = "-"+message.replace("Start","End")+"-"
        return message
    # Decorator core
    def inner(*args, **kwargs):
        name = function.__name__
        print('o'*lenght)
        print(print_message(name, end=False))
        function(*args, **kwargs)
        print(print_message(name, end=True))
        print('x'*lenght)
    inner.nodeco = function
    return inner