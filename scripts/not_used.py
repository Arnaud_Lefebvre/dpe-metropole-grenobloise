"""
Container for unused script parts (in case want to reuse)
"""
# Previous transform geojson IRIS coordinates on communes+levenshtein
conn = sqlite3.connect('dpe.db')
cur = conn.cursor()
cur.execute("""SELECT nom FROM communes""")
communes_names = [fetched[0] for fetched in cur.fetchall()]
cur.execute("""SELECT geojson, nom_commune, iris FROM iris""")
iris_data = [fetched[0:3] for fetched in cur.fetchall()]
with open ("./datasets/coordonnees_iris_transformees.csv","w") as dump_file:
    dump_file.write("geojson_transforme_en_lat_lon,nom_commune,iris\n")
    commune_found = False
    i=0
    for iris_row in iris_data:
        i+=1
        for commune in communes_names:
            ratio = Levenshtein.ratio(iris_row[1], commune) #did it with communes+levenshtein but would have been better with code_insee (likely)
            if ratio >= 0.75:
                commune_found = True
                break
        if commune_found:
            print(f"ligne {i}: Une commune a été trouvée, transformation des coordonnées en cours...")
            rewritten_geojson = rewrite_geojson(iris_row[0])
            dump_file.write(rewritten_geojson+","+iris_row[1]+","+iris_row[2]+"\n")
        commune_found = False
conn.commit()
cur.close()
conn.close()