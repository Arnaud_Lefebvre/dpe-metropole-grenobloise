"""
Compares update stats to know whether an update occured
"""
#

#

from scripts.splitter_decorator import splitter_decorator
from scripts.update_get_stats_api import update_get_stats_api
from scripts.update_get_stats_local import update_get_stats_local
import scripts.variables


@splitter_decorator
def update_compare_stats():
    """
    Compares update stats to know whether an update occured
    """
    difference_count = sum([
        not(scripts.variables.shared_old_last_update_date == scripts.variables.shared_new_last_update_date),
        not(scripts.variables.shared_old_last_data_date == scripts.variables.shared_new_last_data_date),
        not(scripts.variables.shared_old_total_lines == scripts.variables.shared_new_total_lines),
        not(scripts.variables.shared_old_total_unique_id_values == scripts.variables.shared_new_total_unique_id_values),
        not(scripts.variables.shared_old_total_unique_numero_dpe_values == scripts.variables.shared_new_total_unique_numero_dpe_values)
    ])
    # natural cases
    # case nothing changed
    if difference_count == 0:
        print("Pas de mise à jour des données des DPE à télécharger.")
    # case all changed
    elif difference_count == 5:
        print("Les données des DPE ont été mises à jour. La mise à jour va donc être téléchargée puis traitée.")
        scripts.variables.update_was_made = True

    # unnatural cases only some changed
    # probably useless
    # hence easy way out: prompt user to do smth
    else:
        print("Les valeurs de mise à jour sont mauvaises, veuillez vous saisir du problème si vous voulez télécharger la mise à jour potentielle.")
        raise Exception("Mauvaises valeurs de mise à jour de l'API ADEME pour les DPE")
    # else
    # check three counts are equal - they should
    # case date change but no count change - deleted lines..? - update anyway ? - re-download full csv ? - get all ids and compare
    # case count change but no date change - update happened but is not displayed - update anyways and set update date to today instead of API's date ?
    #               or set specific variable date is not to be trusted and leave it be until next time - add script to handle it