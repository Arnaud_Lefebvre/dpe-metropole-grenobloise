"""
Gets current intel from DPE API to know whether an update occured
"""
import dateutil
from dateutil import parser
import json

import requests

from scripts.splitter_decorator import splitter_decorator
import scripts.variables


@splitter_decorator
def update_get_stats_api():
    """
    Gets intel from DPE API to know whether an update occured
    """
    # variables expected : last_update_date, last_data_date, total_lines, total_unique_id_values, total_unique_numero_dpe_values
    if (
        (scripts.variables.shared_new_last_update_date is None) or
        (scripts.variables.shared_new_last_data_date is None) or
        (scripts.variables.shared_new_total_lines is None) or
        (scripts.variables.shared_new_total_unique_id_values is None) or
        (scripts.variables.shared_new_total_unique_numero_dpe_values is None)
    ): #if not all values already exist in shared variables
        print("Requêtage de l'API pour vérification de la version des données des DPE...")
        dpe_api_infos_dict = json.loads(requests.get(
            "https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/"
        ).text)
        dpe_api_one_line_last_date = json.loads(requests.get(
            "https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/lines"
            "?page=1"
            "&size=1"
            "&select=date_reception_dpe"
            "&sort=-date_reception_dpe"
        ).text)
        scripts.variables.shared_new_last_update_date = str(dateutil.parser.parse(dpe_api_infos_dict.get('updatedAt')))
        scripts.variables.shared_new_last_data_date = str(dateutil.parser.parse(dpe_api_one_line_last_date.get('results')[0].get('date_reception_dpe')))
        scripts.variables.shared_new_total_lines = str(int(dpe_api_one_line_last_date.get('total')))
        scripts.variables.shared_new_total_unique_id_values = str(int(dpe_api_infos_dict.get('schema')[0].get('x-cardinality')))
        scripts.variables.shared_new_total_unique_numero_dpe_values = str(int(dpe_api_infos_dict.get('schema')[1].get('x-cardinality')))