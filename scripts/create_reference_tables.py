"""
Creates reference tables for DPE data manually, from values found in API doc
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def create_reference_tables():
    """
    Creates reference tables for DPE data manually, from values found in API doc
    """
    def create_reference_table_tr002_type_batiment_id():
        """
        Creates reference table for building type
        """
        print("Création de la table de référence pour le type de bâtiment...")
        conn, cur = connection_open()
        cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='tr002_type_batiment'""")
        try:
            if not cur.fetchone()[0] == "tr002_type_batiment":
                print("La table <tr002_type_batiment> n'existe pas : création de la table <tr002_type_batiment>...")
                cur.execute("""CREATE TABLE tr002_type_batiment(id INT, type_batiment TEXT, type_batiment_detaille)""")
                cur.execute("""
                    INSERT INTO tr002_type_batiment
                    VALUES
                        (1, "Maison", "Maison individuelle"),
                        (2, "Appartement", "Logement"),
                        (3, "Logements collectifs", "Bâtiment collectif à usage principal d'habitation"),
                        (4, "Non résidentiel", "Bâtiments ou partie de bâtiment à usage principal autre que d'habitation"),
                        (5, "Centres commerciaux", "Centres Commerciaux")
                """)
        except TypeError as error:
            name = str(error)
            if not name == "'NoneType' object is not subscriptable":
                raise
            else:
                print("La table <tr002_type_batiment> n'existe pas : création de la table <tr002_type_batiment>...")
                cur.execute("""CREATE TABLE tr002_type_batiment(id INT, type_batiment TEXT, type_batiment_detaille)""")
                cur.execute("""
                    INSERT INTO tr002_type_batiment
                    VALUES
                        (1, "Maison", "Maison individuelle"),
                        (2, "Appartement", "Logement"),
                        (3, "Logements collectifs", "Bâtiment collectif à usage principal d'habitation"),
                        (4, "Non résidentiel", "Bâtiments ou partie de bâtiment à usage principal autre que d'habitation"),
                        (5, "Centres commerciaux", "Centres Commerciaux")
                """)
                connection_close()
        print("Fin de la création de la table de référence pour le type de bâtiment.")
    def create_reference_table_tr001_modele_dpe():
        """
        Creates reference table for dpe model
        """
        print("Création de la table de référence pour les modèles DPE...")
        conn, cur = connection_open()
        cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='tr001_modele_dpe'""")
        try:
            if not cur.fetchone()[0] == "tr001_modele_dpe":
                print("La table <tr001_modele_dpe> n'existe pas : création de la table <tr001_modele_dpe>...")
                cur.execute("""
                    CREATE TABLE tr001_modele_dpe(
                        id INT,
                        code TEXT,
                        type TEXT,
                        modele TEXT,
                        description TEXT
                    )
                """)
                cur.execute("""
                    INSERT INTO tr001_modele_dpe
                    VALUES
                        (1, "V1", "1", "6.1", "Pour les bâtiments à usage principal d'habitation : Consommations estimées (consommation conventionnelle)"),
                        (2, "V2", "1", "6.2", "Pour les bâtiments à usage principal d'habitation : Consommations réelles (Méthode Facture)"),
                        (3, "V3", "1", "6.3a", "Pour les bâtiments à usage principal de bureau, d'administration ou d'enseignement : Consommation par Usage"),
                        (4, "V4", "1", "6.3a bis", "Pour les bâtiments à usage principal de bureau, d'administration ou d'enseignement : Consommation par Energie"),
                        (5, "V5", "1", "6.3b", "Pour les bâtiments à occupation continue : Consommation par Usage"),
                        (6, "V6", "1", "6.3b bis", "Pour les bâtiments à occupation continue : Consommation par Energie"),
                        (7, "V7", "1", "6.3c", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Usage"),
                        (8, "V8", "1", "6.3c bis", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Energie"),
                        (9, "L1", "4", "6.A", "Pour les bâtiments à usage principal d’habitation : Consommations estimées (consommation conventionnelle)"),
                        (10, "L2", "4", "6.B", "Pour les bâtiments à usage principal d’habitation : Consommations réelles (Méthode Facture)"),
                        (11, "N1", "3", "6.1 neuf", "Pour les bâtiments neufs à usage principal d’habitation"),
                        (12, "N2", "3", "6.2 neuf", "Pour les bâtiments à usage autre que d’habitation"),
                        (14, "P1", "2", "6.1 public", "Pour les bâtiments à usage principal de bureau, d’administration ou d’enseignement : Consommation par Usage"),
                        (15, "P2", "2", "6.1 bis public", "Pour les bâtiments à usage principal de bureau, d’administration ou d’enseignement : Consommation par Energie"),
                        (16, "P3", "2", "6.2 public", "Pour les bâtiments à occupation continue : Consommation par Usage"),
                        (17, "P4", "2", "6.2 bis public", "Pour les bâtiments à occupation continue : Consommation par Energie"),
                        (18, "P5", "2", "6.3", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Usage"),
                        (19, "P6", "2", "6.3 bis public", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Energie"),
                        (20, "C1", "5", "Par usage", "Pour les centres commerciaux"),
                        (21, "C2", "5", "Par énergie", "Pour les centres commerciaux"),
                        (22, "I1", "6", "6.1 (Vente)", "Pour les bâtiments à usage principal d’habitation : Consommations estimées (consommation conventionnelle)"),
                        (23, "I2", "6", "6.2 (Vente)", "Pour les bâtiments à usage principal d’habitation : Consommations réelles (Méthode Facture)"),
                        (24, "XX", "7", NULL, "Autres cas : Demande de subventions, baux commerciaux, ...")
                """)
        except TypeError as error:
            name = str(error)
            if not name == "'NoneType' object is not subscriptable":
                raise
            else:
                print("La table <tr001_modele_dpe> n'existe pas : création de la table <tr001_modele_dpe>...")
                cur.execute("""
                    CREATE TABLE tr001_modele_dpe(
                        id INT,
                        code TEXT,
                        type TEXT,
                        modele TEXT,
                        description TEXT
                    )
                """)
                cur.execute("""
                    INSERT INTO tr001_modele_dpe
                    VALUES
                        (1, "V1", "1", "6.1", "Pour les bâtiments à usage principal d'habitation : Consommations estimées (consommation conventionnelle)"),
                        (2, "V2", "1", "6.2", "Pour les bâtiments à usage principal d'habitation : Consommations réelles (Méthode Facture)"),
                        (3, "V3", "1", "6.3a", "Pour les bâtiments à usage principal de bureau, d'administration ou d'enseignement : Consommation par Usage"),
                        (4, "V4", "1", "6.3a bis", "Pour les bâtiments à usage principal de bureau, d'administration ou d'enseignement : Consommation par Energie"),
                        (5, "V5", "1", "6.3b", "Pour les bâtiments à occupation continue : Consommation par Usage"),
                        (6, "V6", "1", "6.3b bis", "Pour les bâtiments à occupation continue : Consommation par Energie"),
                        (7, "V7", "1", "6.3c", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Usage"),
                        (8, "V8", "1", "6.3c bis", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Energie"),
                        (9, "L1", "4", "6.A", "Pour les bâtiments à usage principal d’habitation : Consommations estimées (consommation conventionnelle)"),
                        (10, "L2", "4", "6.B", "Pour les bâtiments à usage principal d’habitation : Consommations réelles (Méthode Facture)"),
                        (11, "N1", "3", "6.1 neuf", "Pour les bâtiments neufs à usage principal d’habitation"),
                        (12, "N2", "3", "6.2 neuf", "Pour les bâtiments à usage autre que d’habitation"),
                        (14, "P1", "2", "6.1 public", "Pour les bâtiments à usage principal de bureau, d’administration ou d’enseignement : Consommation par Usage"),
                        (15, "P2", "2", "6.1 bis public", "Pour les bâtiments à usage principal de bureau, d’administration ou d’enseignement : Consommation par Energie"),
                        (16, "P3", "2", "6.2 public", "Pour les bâtiments à occupation continue : Consommation par Usage"),
                        (17, "P4", "2", "6.2 bis public", "Pour les bâtiments à occupation continue : Consommation par Energie"),
                        (18, "P5", "2", "6.3", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Usage"),
                        (19, "P6", "2", "6.3 bis public", "Pour les autres bâtiments non mentionnés dans les deux précédents cas : Consommation par Energie"),
                        (20, "C1", "5", "Par usage", "Pour les centres commerciaux"),
                        (21, "C2", "5", "Par énergie", "Pour les centres commerciaux"),
                        (22, "I1", "6", "6.1 (Vente)", "Pour les bâtiments à usage principal d’habitation : Consommations estimées (consommation conventionnelle)"),
                        (23, "I2", "6", "6.2 (Vente)", "Pour les bâtiments à usage principal d’habitation : Consommations réelles (Méthode Facture)"),
                        (24, "XX", "7", NULL, "Autres cas : Demande de subventions, baux commerciaux, ...")
                """)
                connection_close()
        print("Fin de la création de la table de référence pour les modèles de DPE.")

    if "tr002_type_batiment" in scripts.variables.task_list_reference_tables:
        create_reference_table_tr002_type_batiment_id()
    if "tr001_modele_dpe" in scripts.variables.task_list_reference_tables:
        create_reference_table_tr001_modele_dpe()