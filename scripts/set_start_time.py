"""
Sets-up variable start_time for use in splitter_decorator script
"""
import time

#

import scripts.variables


def set_start_time():
    """
    Sets-up shared variable start_time for use in splitter_decorator script
    """
    if scripts.variables.start_time == None:
        scripts.variables.start_time = time.time()
    return