"""
Everything needed for deployment should be here
"""
import base64
import json
import sqlite3

import folium
from folium import IFrame
from folium import plugins
import matplotlib.pyplot
import mpld3
import pandas
import seaborn

from scripts.db_connection_handler import connection_open, connection_close


ce_colors = ['#118811', '#44BB11', '#AADD11', '#FFFF11', '#FFB411', '#FF6611', '#FF0011']
eges_colors = ['#EEDDFF', '#DDCCDD', '#CCBBBB', '#BB99B2', '#9977AA', '#775588', '#553377']
classes = ["A", "B", "C", "D", "E", "F", "G"]
letters = "ABCDEFG"
def get_dpe_color(dpe_type: str, dpe_class: str) -> str:
    """
    In  DPE type ("ce" or "eges")
        DPE class ("A", "B", "C", "D", "E", "F" or "G")
    Out DPE color for display
    """
    switcher = {
        "ce": {
            "A": '#118811',
            "B": '#44BB11',
            "C": '#AADD11',
            "D": '#FFFF11',
            "E": '#FFB411',
            "F": '#FF6611',
            "G": '#FF0011'
        },
        "eges": {
            "A": '#EEDDFF',
            "B": '#DDCCDD',
            "C": '#CCBBBB',
            "D": '#BB99B2',
            "E": '#9977AA',
            "F": '#775588',
            "G": '#553377'
        }
    }
    return switcher.get(dpe_type).get(dpe_class)
def get_actual_class(dpe_type: str, amount: float) -> str:
    """
    In  DPE type ("ce" or "eges") 
        DPE absolute (float)
    Out DPE class (str "A", "B", "C", "D", "E", "F" or "G")
    """
    switcher = {
        "ce": [50, 90, 150, 230, 330, 450],
        "eges": [5, 10, 20, 35, 55, 80]
    }
    dpe_ref = switcher.get(dpe_type)
    classes = "ABCDEF"
    for i in range(6):
        if amount <= dpe_ref[i]:
            return classes[i]
    return "G"
conn, cur = connection_open()

cur.execute("""
    SELECT CAST(surface_habitable AS INT) as surface, AVG(consommation_energie), AVG(estimation_ges)
    FROM dpe
    WHERE re_code_iris IS NOT NULL
        AND consommation_energie < 4000
        AND consommation_energie > 0
        AND estimation_ges < 1000
        AND estimation_ges > 0
    GROUP BY surface
""")
dpe_par_surface = [{
    "surface": wxc[0],
    "ce": wxc[1],
    "eges": wxc[2]
} for wxc in cur.fetchall()]

cur.execute("""
    SELECT annee_construction, AVG(consommation_energie) as avg_ce, AVG(estimation_ges) as avg_eges, count(*)
    FROM dpe
    WHERE re_code_iris IS NOT NULL
        AND consommation_energie < 4000
        AND consommation_energie > 0
        AND estimation_ges < 1000
        AND estimation_ges > 0
        AND annee_construction < 2022
        AND annee_construction >= 1900
    GROUP BY annee_construction
        HAVING avg_ce > 0
        AND avg_eges > 0
    ORDER BY annee_construction
""")
dpe_par_annee = [{
    "annee": qsd[0],
    "avg_ce": qsd[1],
    "avg_eges": qsd[2],
    "count": qsd[3],
} for qsd in cur.fetchall()]

cur.execute("""
    SELECT type_batiment, AVG(consommation_energie) as avg, count(*)
    FROM dpe
        JOIN tr002_type_batiment
            ON dpe.tr002_type_batiment_id = tr002_type_batiment.id
    WHERE re_code_iris IS NOT NULL
        AND consommation_energie < 4000
        AND consommation_energie > 0
        AND estimation_ges < 1000
        AND estimation_ges > 0
    GROUP BY type_batiment
""")
dpe_par_type = [{
    "type": qsd[0],
    "avg": qsd[1],
    "count": qsd[2],
} for qsd in cur.fetchall()]

cur.execute("""
    SELECT re_code_iris, consommation_energie, estimation_ges, classe_consommation_energie, classe_estimation_ges
    FROM dpe
    WHERE re_code_iris IS NOT NULL
        AND consommation_energie < 4000
        AND consommation_energie > 0
        AND estimation_ges < 1000
        AND estimation_ges > 0
""")
# cur.execute("""
#     SELECT re_code_iris, consommation_energie, estimation_ges, classe_consommation_energie, classe_estimation_ges, tr001_modele_dpe.modele, tr001_modele_dpe.description
#     FROM dpe
#         LEFT JOIN tr001_modele_dpe
#             ON tr001_modele_dpe.id=dpe.tr001_modele_dpe_id
#     WHERE re_code_iris IS NOT NULL
#         AND consommation_energie < 4000
#         AND consommation_energie > 0
#         AND estimation_ges < 1000
#         AND estimation_ges > 0
# """)
# results = [{
#     "re_code_iris": fetched[0],
#     "consommation_energie": float(fetched[1]),
#     "estimation_ges": float(fetched[2]),
#     "classe_consommation_energie": fetched[3],
#     "classe_estimation_ges": fetched[4],
#     "modele_dpe": fetched[5],
#     "modele_dpe_desc": fetched[6]
# } for fetched in cur.fetchall()]
results = [{
    "re_code_iris": fetched[0],
    "consommation_energie": float(fetched[1]),
    "estimation_ges": float(fetched[2]),
    "classe_consommation_energie": fetched[3],
    "classe_estimation_ges": fetched[4]
} for fetched in cur.fetchall()]

cur.execute("""
    SELECT code_iris
    FROM iris
        JOIN communes
            ON communes.code_insee=iris.code_insee
""")
iris_codes = [fetched[0] for fetched in cur.fetchall()]
data_dict = {}
for iris_code in iris_codes:
    data_dict[iris_code] = {
        "count": 0,
        "ce_total": 0,
        "eges_total": 0,
        "ce_classes_count": {
            "A": 0,
            "B": 0,
            "C": 0,
            "D": 0,
            "E": 0,
            "F": 0,
            "G": 0
        },
        "eges_classes_count": {
            "A": 0,
            "B": 0,
            "C": 0,
            "D": 0,
            "E": 0,
            "F": 0,
            "G": 0
        }
    }
by_class_dicts_list = []
for letter in letters:
    for letterer in letters:
        by_class_dicts_list.append(
            {
                "ce": letter,
                "eges": letterer,
                "count": 0
            }
        )
for result in results:
    actualce = get_actual_class("ce", result["consommation_energie"])
    actualeges = get_actual_class("eges", result["estimation_ges"])

    index = letters.index(actualce)*7 + letters.index(actualeges)
    by_class_dicts_list[index]["count"] += 1

    data_dict[result["re_code_iris"]]["count"] += 1
    data_dict[result["re_code_iris"]]["ce_total"] += result["consommation_energie"]
    data_dict[result["re_code_iris"]]["eges_total"] += result["estimation_ges"]
    data_dict[result["re_code_iris"]]["ce_classes_count"][actualce] += 1
    data_dict[result["re_code_iris"]]["eges_classes_count"][actualeges] += 1
metro_dict = {
    "count": 0,
    "ce_total": 0,
    "eges_total": 0,
    "ce_classes_count": {
        "A": 0,
        "B": 0,
        "C": 0,
        "D": 0,
        "E": 0,
        "F": 0,
        "G": 0
    },
    "eges_classes_count": {
        "A": 0,
        "B": 0,
        "C": 0,
        "D": 0,
        "E": 0,
        "F": 0,
        "G": 0
    }
}
for data_code_iris in data_dict:
    metro_dict["count"] += data_dict[data_code_iris]["count"]
    metro_dict["ce_total"] += data_dict[data_code_iris]["ce_total"]
    metro_dict["eges_total"] += data_dict[data_code_iris]["eges_total"]
    for letter in "ABCDEFG":
        metro_dict["ce_classes_count"][letter] += data_dict[data_code_iris]["ce_classes_count"][letter]
        metro_dict["eges_classes_count"][letter] += data_dict[data_code_iris]["eges_classes_count"][letter]
cur.execute("""
    SELECT code_iris, nom_iris, iris.code_insee, nom_commune, re_wgs_geojson
    FROM iris
        JOIN communes
            ON communes.code_insee=iris.code_insee
""")
iris_data = [{
    "code_iris": fetched[0],
    "nom_iris": fetched[1],
    "code_insee": fetched[2],
    "nom_commune": fetched[3],
    "re_wgs_geojson": fetched[4]
} for fetched in cur.fetchall()]

def do_first_map():
    """
    Returns first map
    """
    map_repartition = folium.plugins.DualMap(
        location=(45.18,5.72),
        tiles=None,
        zoom_start=11,
        zoom_control=True
    )
    folium.TileLayer(
        tiles='openstreetmap', name="Open Street Map", overlay=False, control=False
    ).add_to(map_repartition)
    fg_transparent_m1 = folium.FeatureGroup(
        name="Transparent",
        overlay=False,
        control=True,
        show=True,
    )
    fg_transparent_m2 = folium.FeatureGroup(
        name="Transparent",
        overlay=False,
        control=True,
        show=True,
    )
    fg_opaque_m1 = folium.FeatureGroup(
        name="Opaque",
        overlay=False,
        control=True,
        show=True,
    )
    fg_opaque_m2 = folium.FeatureGroup(
        name="Opaque",
        overlay=False,
        control=True,
        show=True,
    )
    actual_color = ""
    plot = None
    for iris_row in iris_data:
        # CE
        try:
            code_iris = iris_row["code_iris"]

            keys = data_dict[code_iris]["ce_classes_count"].keys()
            values = data_dict[code_iris]["ce_classes_count"].values()
            seaborn.set_palette(seaborn.color_palette(ce_colors))
            fig, ax = matplotlib.pyplot.subplots(figsize=(6,6))
            data = pandas.DataFrame(list(zip(keys,values)),
                                columns=['Classes de consommation d\'énergie','Quantité'])
            seaborn.barplot(data=data, y='Classes de consommation d\'énergie',x='Quantité')
            ax.set_title(f"Commune : {iris_row['nom_commune']}, IRIS : {iris_row['nom_iris']}")
            ax.set_ylabel("Classes de consommation d\'énergie", fontsize="20")
            ax.set_xlabel("Quantité", fontsize="20")
            matplotlib.pyplot.savefig("graphs/some.png", dpi=50)
            matplotlib.pyplot.close(fig)
            with open("graphs/some.png", "rb") as file:
                encoded = base64.b64encode(file.read())
            html = '<img src="data:image/png;base64,{}">'.format
            iframe = IFrame(html(encoded.decode('UTF-8')), width=320, height=320)
            popup = folium.Popup(iframe, max_width=320)
            popup2 = folium.Popup(iframe, max_width=320)

            average = data_dict[code_iris]["ce_total"] / data_dict[code_iris]["count"]
            actual_class = get_actual_class("ce", average)
            actual_color = get_dpe_color("ce", actual_class)
            weight = 1
            line_color = "grey"
            fill_opacity = 0.45
            tt = folium.Tooltip(f"Commune : {iris_row['nom_commune']}<br>"+ \
                        f"IRIS : {iris_row['nom_iris']}<br>"+ \
                        f"Code INSEE : {iris_row['code_insee']}<br>"+ \
                        f"Code IRIS : {code_iris}<br>"+ \
                        f"Moyenne CE : {round(average, 2)}kWhEP/m².an<br>"+ \
                        f"Classe moyenne : {actual_class}")
            def style_function_func(feature, weight=weight, line_color=line_color, color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": weight,
                    "color": line_color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            gj = folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=True,
                overlay=False,
                zoom_on_click=False,
            )
            tt.add_to(gj)
            popup.add_to(gj)
            gj.add_to(fg_transparent_m1)
            weight = 1.6
            line_color = "black"
            fill_opacity = 1
            def style_function_func(feature, weight=weight, line_color=line_color, color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": weight,
                    "color": line_color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            gj = folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=True,
                overlay=False,
                zoom_on_click=False,
            )
            tt = folium.Tooltip(f"Commune : {iris_row['nom_commune']}<br>"+ \
                        f"IRIS : {iris_row['nom_iris']}<br>"+ \
                        f"Code INSEE : {iris_row['code_insee']}<br>"+ \
                        f"Code IRIS : {code_iris}<br>"+ \
                        f"Moyenne CE : {round(average, 2)}kWhEP/m².an<br>"+ \
                        f"Classe moyenne : {actual_class}")
            tt.add_to(gj)
            popup2.add_to(gj)
            gj.add_to(fg_opaque_m1)
        except ValueError:
            pass
        except (KeyError, ZeroDivisionError):
            actual_color = "#000000"
            fill_opacity = 0.2
            def style_function_func(feature,color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": 0.5,
                    "color": color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=False,
                tooltip=f"{code_iris}<br>Données manquantes et/ou erronées",
            ).add_to(map_repartition.m1)
        # EGES
        try:
            code_iris = iris_row["code_iris"]
        
            keys = data_dict[code_iris]["eges_classes_count"].keys()
            values = data_dict[code_iris]["eges_classes_count"].values()
            seaborn.set_palette(seaborn.color_palette(eges_colors))
            fig, ax = matplotlib.pyplot.subplots(figsize=(6,6))
            data = pandas.DataFrame(list(zip(keys,values)),
                                columns=['Classes d\'estimation d\'émissions de GES','Quantité'])
            seaborn.barplot(data=data, y='Classes d\'estimation d\'émissions de GES',x='Quantité')
            ax.set_title(f"Commune : {iris_row['nom_commune']}, IRIS : {iris_row['nom_iris']}")
            ax.set_ylabel("Classes d\'estimation d\{émissions de GES", fontsize="20")
            ax.set_xlabel("Quantité", fontsize="20")
            matplotlib.pyplot.savefig("graphs/some2.png", dpi=50)
            matplotlib.pyplot.close(fig)
            with open("graphs/some2.png", "rb") as file:
                encoded = base64.b64encode(file.read())
            html = '<img src="data:image/png;base64,{}">'.format
            iframe = IFrame(html(encoded.decode('UTF-8')), width=320, height=320)
            popup = folium.Popup(iframe, max_width=320)
            popup2 = folium.Popup(iframe, max_width=320)

            average = data_dict[code_iris]["eges_total"] / data_dict[code_iris]["count"]
            actual_class = get_actual_class("eges", average)
            actual_color = get_dpe_color("eges", actual_class)
            weight = 1
            line_color = "grey"
            fill_opacity = 0.45
            tt = folium.Tooltip(f"Commune : {iris_row['nom_commune']}<br>"+ \
                        f"IRIS : {iris_row['nom_iris']}<br>"+ \
                        f"Code INSEE : {iris_row['code_insee']}<br>"+ \
                        f"Code IRIS : {code_iris}<br>"+ \
                        f"Moyenne CE : {round(average, 2)}kWhEP/m².an<br>"+ \
                        f"Classe moyenne : {actual_class}")
            def style_function_func(feature, weight=weight, line_color=line_color, color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": weight,
                    "color": line_color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            gj = folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=True,
                overlay=False,
                zoom_on_click=False,
            )
            tt.add_to(gj)
            popup.add_to(gj)
            gj.add_to(fg_transparent_m2)
            weight = 1.6
            line_color = "black"
            fill_opacity = 1
            def style_function_func(feature, weight=weight, line_color=line_color, color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": weight,
                    "color": line_color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            gj = folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=False,
                overlay=False,
                zoom_on_click=False,
            )
            tt = folium.Tooltip(f"Commune : {iris_row['nom_commune']}<br>"+ \
                        f"IRIS : {iris_row['nom_iris']}<br>"+ \
                        f"Code INSEE : {iris_row['code_insee']}<br>"+ \
                        f"Code IRIS : {code_iris}<br>"+ \
                        f"Moyenne CE : {round(average, 2)}kWhEP/m².an<br>"+ \
                        f"Classe moyenne : {actual_class}")
            tt.add_to(gj)
            popup2.add_to(gj)
            gj.add_to(fg_opaque_m2)
        except ValueError:
            pass
        except (KeyError, ZeroDivisionError):
            actual_color = "#000000"
            fill_opacity = 0.2
            def style_function_func(feature,color=actual_color, fillOpacity=fill_opacity):
                return {
                    "opacity": 1,
                    "weight": 0.5,
                    "color": color,
                    "fillColor": color,
                    "fillOpacity": fillOpacity,
                    "fill": True
                }
            folium.GeoJson(
                data=iris_row["re_wgs_geojson"],
                style_function=style_function_func,
                name=code_iris,
                show=True,
                control=False,
                overlay=False,
                tooltip=f"{code_iris}<br>Données manquantes et/ou erronées",
            ).add_to(map_repartition.m2)
    fg_transparent_m1.add_to(map_repartition.m1)
    fg_opaque_m1.add_to(map_repartition.m1)
    fg_transparent_m2.add_to(map_repartition.m2)
    fg_opaque_m2.add_to(map_repartition.m2)
    cur.execute("""
        SELECT geojson
        FROM communes
    """)
    raw_communes_geojsons = [fetched[0] for fetched in cur.fetchall()]
    communes_geojsons = {
        "type": "FeatureCollection",
        "features": []
    }
    for raw_commune_geojson in raw_communes_geojsons:
        commune_geojson = {
            "type": "Feature",
            "geometry": {
                'type': 'Polygon',
                'coordinates': json.loads(raw_commune_geojson).get('geometry'),
            },
        }
        communes_geojsons["features"].append(commune_geojson)
    def piste_style_function_zero(feature):
        return {'opacity': 1,
                'weight': 2.6,
                'color': "black",
                'fill': False}
    contours_communes = folium.GeoJson(
        communes_geojsons,
        style_function=piste_style_function_zero,
        name = "Contours des communes",
        show=False,
        control=True,
    )
    contours_communes.add_to(map_repartition)
    folium.LayerControl(name="lc", collapsed=False, autoZIndex=False).add_to(map_repartition)
    folium.plugins.FloatImage(image="classes_ce.png", bottom=0, left=0).add_to(map_repartition)
    folium.plugins.FloatImage(image="classes_eges.png", bottom=0, left=50).add_to(map_repartition)
    

    connection_close()
    return map_repartition

def do_compare_map():
    # avant data
    conn, cur = connection_open()

    cur.execute(
        """SELECT longitude, latitude, COUNT(*)
            FROM dpe
            GROUP BY longitude, latitude"""
    )
    dpe_table_grouped_coord_avant = [{
            "longitude": res[0],
            "latitude": res[1],
            "lines_count": res[2]
        } for res in cur.fetchall()]
    # après data
    cur.execute(
        """SELECT re_geo_longitude, re_geo_latitude, COUNT(*)
            FROM dpe
            GROUP BY re_geo_longitude, re_geo_latitude"""
    )
    dpe_table_grouped_coord_apres = [{
            "longitude": res[0],
            "latitude": res[1],
            "lines_count": res[2]
        } for res in cur.fetchall()]
    # both map
    map_compare = folium.plugins.DualMap(location=[47,2.2], zoom_start=5, zoom_control=True, tiles=None, prefer_canvas=True)
    folium.TileLayer(tiles='openstreetmap', overlay=True, control=False).add_to(map_compare.m1)
    folium.TileLayer(tiles='openstreetmap', overlay=True, control=False).add_to(map_compare.m2)
    # mav = folium.Map(location=(45,5), zoom_start=11)
    icon_create_function = '''
        function(cluster) {
            var childCount = cluster.getChildCount();

            var c = ' marker-cluster-';
            var markerColor;
            if (childCount < 100) {
                c += 'small';
                marker_color = '#6dcd2c66';
            } else if (childCount < 1000) {
                c += 'medium';
                marker_color = '#d2b71b66';
            } else {
                c += 'large';
                marker_color = '#ff8e1166';
            }

            return new L.DivIcon({
                html: `<div style="background-color:${marker_color}"><span>${childCount}</span></div>`,
                className: 'marker-cluster' + c,
                iconSize: (40, 40)
            });

        }
    '''
    max_cluster_radius = 120
    cluster_avant = folium.plugins.MarkerCluster(
        name="Avant",
        overlay=False,
        options={
            "maxClusterRadius":max_cluster_radius,
            "chunkedLoading":True
            },
        icon_create_function=icon_create_function
        )
    cluster_apres = folium.plugins.MarkerCluster(
        name="Après",
        overlay=False,
        options={
            "maxClusterRadius":max_cluster_radius,
            "chunkedLoading":True
            },
        icon_create_function=icon_create_function
    )

    for dpe_avant in dpe_table_grouped_coord_avant:
        try:
            folium.CircleMarker(
                location=(dpe_avant.get('latitude'),dpe_avant.get('longitude')),
                radius=1.5+int(dpe_avant.get('lines_count'))*0.5
            ).add_to(cluster_avant)
        except ValueError:
            pass
    for dpe_apres in dpe_table_grouped_coord_apres:
        try:
            folium.CircleMarker(
                location=(dpe_apres.get('latitude'),dpe_apres.get('longitude')),
                radius=1.5+int(dpe_apres.get('lines_count'))*0.5
            ).add_to(cluster_apres)
        except ValueError:
            pass

    cluster_avant.add_to(map_compare.m1)
    cluster_apres.add_to(map_compare.m2)
    folium.LayerControl(collapsed=False).add_to(map_compare)
    return map_compare

def do_repartition_ce_metro():
    colors = ['#118811', '#44BB11', '#AADD11', '#FFFF11', '#FFB411', '#FF6611', '#FF0011']
    seaborn.set_palette(seaborn.color_palette(ce_colors))
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    data = pandas.DataFrame(list(zip(classes,metro_dict["ce_classes_count"].values())),
                        columns=['Classes de consommation d\'énergie','Quantité'])
    seaborn.barplot(data=data, y='Classes de consommation d\'énergie',x='Quantité')
    ax.set_ylabel("Classes de consommation d\'énergie", fontsize="20")
    ax.set_xlabel("Quantité", fontsize="20")
    matplotlib.pyplot.show()
def do_repartition_eges_metro():
    colors = ['#EEDDFF', '#DDCCDD', '#CCBBBB', '#BB99B2', '#9977AA', '#775588', '#553377']
    seaborn.set_palette(seaborn.color_palette(eges_colors))
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    data = pandas.DataFrame(list(zip(classes, metro_dict["eges_classes_count"].values())),
                        columns=['Classes d\'estimation d\'émissions de GES', 'Quantité'])
    seaborn.barplot(data=data, y='Classes d\'estimation d\'émissions de GES', x='Quantité')
    ax.set_ylabel("Classes d\'estimation d\'émissions de GES", fontsize="20")
    ax.set_xlabel("Quantité", fontsize="20")
    matplotlib.pyplot.show()
def do_dpe_repartition():
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    colorit = {
        "A": '#118811',
        "B": '#44BB11',
        "C": '#AADD11',
        "D": '#DDDD00',
        "E": '#FFB411',
        "F": '#FF6611',
        "G": '#FF0011',
    }

    for metrow in by_class_dicts_list:
        ax.scatter(
            metrow["ce"],
            metrow["eges"],
            s=metrow["count"],
            c=colorit[metrow["ce"]]
        )

    ax.set_xlabel("Classe consommation énergie",
                fontsize='20')
    ax.set_ylabel("Classe estimation émissions GES",
                fontsize='20')
    matplotlib.pyplot.show()

def do_chronological_chart():
    colors = ['#000000']
    seaborn.set_palette(seaborn.color_palette(colors))
    fig,ax = matplotlib.pyplot.subplots(figsize=(16,10))
    df=pandas.DataFrame({'x_axis': [a.get('annee') for a in dpe_par_annee], 'y_axis': [b.get('avg_ce') for b in dpe_par_annee]})
    seaborn.lineplot(
        data=df,
        x='x_axis',
        y='y_axis'
    )
    ax.set_xlabel("Consommation moyenne d'énergie",
                fontsize='20')
    ax.set_ylabel("Année de construction",
                fontsize='20')
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(20))
    matplotlib.pyplot.show()

def do_stream_chart():
    fix, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    df=pandas.DataFrame({
        'x_axis': [a.get('annee') for a in dpe_par_annee],
        'y_axis_ce': [b.get('avg_ce') for b in dpe_par_annee],
        'y_axis_eges': [b.get('avg_eges')*10 for b in dpe_par_annee]
    })
    matplotlib.pyplot.stackplot(
        df['x_axis'],
        df['y_axis_ce'],
        df['y_axis_eges'],
        labels=("CE","EGES"),
        colors=("#69b423", "975b94"),
        baseline="wiggle",
        data=df
    )
    ax.get_yaxis().set_visible(False)
    matplotlib.pyplot.show()

def do_scatter_indiv():
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    scatterplot = ax.scatter(
        [aa.get('consommation_energie') for aa in results],
        [bb.get('estimation_ges') for bb in results],
        s=1,
        c="black"
    )
    ax.set_xlabel("Consommation énergie",
                fontsize='20')
    ax.set_ylabel("Estimation émissions GES",
                fontsize='20')
    ax.set_xlim(0,1000)
    ax.set_ylim(0,200)

    matplotlib.pyplot.show()

def do_type_conso():
    colors = ['#241010', '#102410', '#101024']
    seaborn.set_palette(seaborn.color_palette(colors))
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    dt = pandas.DataFrame({'x_axis': [a.get('type') for a in dpe_par_type],
                        'y_axis': [b.get('avg') for b in dpe_par_type]})
    seaborn.barplot(data=dt, x='x_axis',y='y_axis')
    ax.set_xlabel("Type de bâtiment", fontsize="20")
    ax.set_ylabel("Moyenne de consommation d'énergie", fontsize="20")
    matplotlib.pyplot.show()

def do_type_count():
    colors = ['#241010', '#102410', '#101024']
    seaborn.set_palette(seaborn.color_palette(colors))
    fig, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    dt = pandas.DataFrame({'x_axis': [a.get('type') for a in dpe_par_type],
                        'y_axis': [b.get('count') for b in dpe_par_type]})
    seaborn.barplot(data=dt, x='x_axis',y='y_axis')
    ax.set_xlabel("Type de bâtiment", fontsize="20")
    ax.set_ylabel("Quantité", fontsize="20")
    matplotlib.pyplot.show()

def do_size_chart():
    fix, ax = matplotlib.pyplot.subplots(figsize=(16,10))
    lineplot = ax.plot(
        [aa.get('surface') for aa in dpe_par_surface],
        [bb.get('ce') for bb in dpe_par_surface]
    )
    ax.set_xlabel("Surface habitable",
                fontsize='20')
    ax.set_ylabel("Consommation d'énergie",
                fontsize='20')
    ax.set_xlim(0,200)
    ax.set_ylim(0,1000)
    matplotlib.pyplot.show()