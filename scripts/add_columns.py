"""
Add columns to modded tables if required
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def add_columns():
    """
    Add columns to modded tables if required
    """
    conn, cur = connection_open()

    def add_columns_iris_table():
        """
        Add columns to iris table if required
        """
        print("Début de la verification des colonnes de la table <iris>...")
        for column in scripts.variables.modded_tables_columns_dict['iris']:
            cur.execute(f"""SELECT COUNT(*) AS CNTREC FROM pragma_table_info('iris') WHERE name='{column}';""")
            if not(cur.fetchone()[0] == 1):
                print(f"La colonne <{column}> n'existe pas, création de la colonne <{column}>...")
                cur.execute(f"""ALTER TABLE iris ADD COLUMN {column} TEXT""")
                conn.commit()
        print("Fin de la verification des colonnes de la table <iris>.")
        print()
    def add_columns_communes_table():
        """
        Add columns to communes table if required
        """
        print("Début de la verification des colonnes de la table <communes>...")
        for column in scripts.variables.modded_tables_columns_dict['communes']:
            cur.execute(f"""SELECT COUNT(*) AS CNTREC FROM pragma_table_info('communes') WHERE name='{column}';""")
            if not(cur.fetchone()[0] == 1):
                print(f"La colonne <{column}> n'existe pas, création de la colonne <{column}>...")
                cur.execute(f"""ALTER TABLE communes ADD COLUMN {column} TEXT""")
                conn.commit()
        print("Fin de la verification des colonnes de la table <communes>.")
        print()
    def add_columns_dpe_table():
        """
        Add columns to dpe table if required
        """
        print("Début de la verification des colonnes de la table <dpe>...")
        for column in scripts.variables.modded_tables_columns_dict['dpe']:
            cur.execute(f"""SELECT COUNT(*) AS CNTREC FROM pragma_table_info('dpe') WHERE name='{column}';""")
            if not(cur.fetchone()[0] == 1):
                print(f"La colonne <{column}> n'existe pas, création de la colonne <{column}>...")
                cur.execute(f"""ALTER TABLE dpe ADD COLUMN {column} TEXT""")
                conn.commit()
        print("Fin de la verification des colonnes de la table <dpe>.")
        print()

    add_columns_iris_table()
    add_columns_communes_table()
    add_columns_dpe_table()
    
    connection_close()