"""
Feeds enrichment values into modded table for IRIS
"""
import csv
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
from scripts.transform_handler import transform_handler
from scripts.db_connection_handler import connection_open, connection_close
from scripts.temp_index import create_temp_index, delete_temp_index


@splitter_decorator
def feed_iris_table():
    """
    Feeds enrichment values into modded table for IRIS
    """
    conn, cur = connection_open()
    # Feed nombre_logements from raw_iris_nombres_logements
    cur.execute("""
        UPDATE iris
        SET nombre_logements = (
                SELECT "P17_LOG"
                FROM raw_iris_nombres_logements
                WHERE "IRIS"=iris.code_iris
            )
    """)
    # Feed transformed geojson from datasets/coordonnees_iris_transformees.csv
    create_temp_index("iris", ["code_insee", "code_iris"])
    with open("datasets/coordonnees_iris_transformees.csv", "r") as re_geojson_csv:
        reader = csv.DictReader(re_geojson_csv, delimiter=";")
        for line in reader:
            _one = "'"+str(line['wgs_geojson'])+"'"
            _two = "\""+str(line['code_insee'])+"\""
            _three = "\""+str(line['code_iris'])+"\""
            cur.execute(f"""
                UPDATE iris
                SET re_wgs_geojson = {_one}
                WHERE code_insee = {_two}
                    AND code_iris = {_three}"""
            )
    delete_temp_index()
    # Apply transformations
    transform_handler("iris")

    connection_close()