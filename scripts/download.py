"""
Downloads required files according to planning
"""
import os
import urllib
import zipfile

import py7zr
import requests

from scripts.splitter_decorator import splitter_decorator
import scripts.variables


@splitter_decorator
def download():
    """
    Downloads required files according to planning
    """
    url_dpe_data = "https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/raw"
    url_communes_data = "https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json"
    # url_contours_iris_data = "ftp://Contours_IRIS_ext:ao6Phu5ohJ4jaeji@ftp3.ign.fr/CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01.7z" # Full
    url_contours_iris_data = "ftp://Iris_GE_ext:eeLoow1gohS1Oot9@ftp3.ign.fr/IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01.7z.001" # 38
    url_nombre_logements_iris = "https://www.insee.fr/fr/statistiques/fichier/4799305/base-ic-logement-2017_csv.zip"

    def download_dpe():
        """
        Downloads dpe data
        """
        print("Début du téléchargement des données des diagnostics de performance énergétique...")
        with requests.get(url_dpe_data) as r_dpe_data:
            r_dpe_data.raise_for_status()
            with open("./datasets/dpe_data.csv", "w") as file:
                file.write(r_dpe_data.text)
        print("Les données des diagnostics de performance énergétique semblent avoir été téléchargées sans erreur.")
    def download_communes():
        """
        Downloads communes data
        """
        print("Début du téléchargement des données sur les communes de l'intercommunalité Grenobloise...")
        with requests.get(url_communes_data) as r_communes_data:
            r_communes_data.raise_for_status()
            with open("./datasets/communes_data.json", "w") as file:
                file.write(r_communes_data.text)
        print("Les données sur les communes de l'intercommunalité Grenobloise semblent avoir été téléchargées sans erreur.")
    def download_iris():
        """
        Downloads iris data
        Extracts it if needed
        """
        # Download
        if not(os.path.isfile("./datasets/contours_iris.7z")):
            print("Début du téléchargement des données sur les contours IRIS...")
            urllib.request.urlretrieve(url_contours_iris_data, "./datasets/contours_iris.7z")
            if os.path.isfile("./datasets/contours_iris.7z"):
                if os.path.getsize("./datasets/contours_iris.7z") > 1500000:
                    print("Les données sur les contours IRIS semblent avoir été téléchargées sans erreur.")
                else:
                    print("Un problème semble être survenu durant le téléchargement des données sur les contours IRIS.")
                    raise
            else:
                print("Un problème semble être survenu durant le téléchargement des données sur les contours IRIS.")
                raise
        # Extraction
        if os.path.isfile("./datasets/contours_iris.7z"):
            print("Début de la décompression des données sur les contours IRIS...")
            archive = py7zr.SevenZipFile('./datasets/contours_iris.7z', mode='r')
            archive.extractall(path="./datasets/")
            archive.close()
            if os.path.isdir("./datasets/IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01"):
                print("Les données sur les contours IRIS semblent avoir été décompressées sans erreur.")
            else:
                print("Un problème semble être survenu durant la décompression des données sur les contours IRIS.")
                raise
        else:
            print("L'archive nécessaire n'est pas présente : annulation de la décompression des données sur les contours IRIS.")
            raise
    def download_iris_nombres_logements():
        """
        Downloads iris_nombres_logements data
        """
        # Download
        if not(os.path.isfile("./datasets/base-ic-logement-2017_csv.zip")):
            print("Début du téléchargement des données du nombre de logements par maille IRIS...")
            with requests.get(url_nombre_logements_iris) as r_nombre_logements_iris:
                r_nombre_logements_iris.raise_for_status()
                with open("./datasets/base-ic-logement-2017_csv.zip", "wb") as file:
                    file.write(r_nombre_logements_iris.content)
            print("Les données du nombre de logements par maille IRIS semblent avoir été téléchargées sans erreur.")
        # Extraction
        if os.path.isfile("./datasets/base-ic-logement-2017_csv.zip"):
            print("Début de la décompression des données du nombre de logements par maille IRIS...")
            with zipfile.ZipFile("./datasets/base-ic-logement-2017_csv.zip", 'r') as zip_ref:
                zip_ref.extractall("./datasets/")
            if os.path.isfile("./datasets/base-ic-logement-2017.CSV"):
                print("Les données du nombre de logements par maille IRIS semblent avoir été décompressées sans erreur.")
            else:
                print("Un problème semble être survenu durant la décompression des données du nombre de logements par maille IRIS.")
        else:
            print("L'archive nécessaire n'est pas présente : annulation de la décompression des données sur les contours IRIS.")
            raise

    def download_switch(tasked_file: str):
        """
        Returns function corresponding to file
        """
        switcher = {
            "dpe_data.csv": download_dpe,
            "communes_data.json": download_communes,
            "IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01": download_iris,
            "base-ic-logement-2017.CSV": download_iris_nombres_logements
        }
        return switcher.get(tasked_file)
    print("Début du téléchargement des données manquantes...")
    for i,tasked_file in enumerate(scripts.variables.task_list_files):
        download_switch(tasked_file)() #download-switch returns appropriate function name then exec that function
        if not((i+1) == len(scripts.variables.task_list_files)): #just a splitter
            print()