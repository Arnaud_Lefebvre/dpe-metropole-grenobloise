"""
Feeds enrichment values into modded table for DPE
"""
import csv
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
import scripts.variables
from scripts.db_connection_handler import connection_open, connection_close
from scripts.place_dpe_in_iris import place_dpe_in_iris
from scripts.duplicate_dpe_columns import duplicate_dpe_columns
from scripts.transform_handler import transform_handler
from scripts.geocode_main import geocode_main_py


@splitter_decorator
def feed_dpe_table(ids_list: list, limit_iris_to_metropole: bool):
    """
    Takes ids_list as input
    Applies every transformation for these ids for table DPE
    """
    conn, cur = connection_open()

    duplicate_dpe_columns(scripts.variables.new_ids_list_as_one_string)
    transform_handler("dpe", ids_list)
    geocode_main_py(ids_list)
    place_dpe_in_iris(scripts.variables.new_ids_list, limit_iris_to_metropole)

    connection_close()