"""
Creates modded table communes
"""
import sqlite3

#

from scripts.splitter_decorator import splitter_decorator
from scripts.db_connection_handler import connection_open, connection_close


@splitter_decorator
def create_communes_table():
    conn, cur = connection_open()

    print("Création de la table <communes> à partir des données brutes de la table <raw_communes>...")
    cur.execute("""CREATE TABLE IF NOT EXISTS communes AS SELECT * FROM raw_communes""")
    print("Fin de la création de la table <communes> à partir des données brutes de la table <raw_communes>.")
    print()

    connection_close()