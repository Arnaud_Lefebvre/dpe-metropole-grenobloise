"""
Exports a file.csv with DPE raw data and modded data
"""
import csv
import sqlite3

#

from scripts.db_connection_handler import connection_open, connection_close


def do_csv():
    conn, cur = connection_open()

    cur.execute("""
        SELECT
            raw_dpe.*,
            dpe.re_code_insee_commune,
            dpe.re_code_postal,
            dpe.re_code_iris,
            dpe.re_code_iris_raw_coords,
            dpe.re_numero_rue,
            dpe.re_type_voie,
            dpe.re_nom_rue,
            dpe.re_commune_normalized,
            dpe.re_geo_longitude,
            dpe.re_geo_latitude,
            dpe.re_geo_score,
            dpe.re_geo_numero_rue,
            dpe.re_geo_type_voie,
            dpe.re_geo_nom_rue,
            dpe.re_geo_code_postal,
            dpe.re_geo_commune,
            dpe.re_geo_code_insee_commune,
            dpe.re_geo_context
        FROM raw_dpe
            JOIN dpe
                ON raw_dpe.id=dpe.id
    """)
    full_data = cur.fetchall()
    headers = [a[0] for a in cur.description][1:]
    with open("dpe.csv", "w") as write_file:
        writer = csv.writer(write_file)
        writer.writerow(headers)
        writer.writerows(full_data)

    connection_close()


if __name__ == "__main__":
    do_csv()