# Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

Projet effectué grâce à l'opendata de la Métropole de Grenoble en partenariat avec Simplon.co Grenoble

## Déploiement du projet

https://arnaud_lefebvre.gitlab.io/dpe-metropole-grenobloise/

## Stack utilisé

Python, Git, GitLab, sqlite3, Jupyter Notebook

Librairies Python : requests, py7zr, fiona, pandas, folium, matplotlib, levenshtein, shapely

## Description du contenu

### Dossiers

- `datasets/` : contient les fichiers plats de faible tailles et longs au traitement
- `dist/images/` : contient les images pour la page Web déployée
- `documentation/` : est explicite
- `scripts/` : contient les scripts composants du logiciel

#### En local seulement

- `records/` : contient des fichiers d'informations (e.g.: transformations effectuées) ou de variables persistentes (e.g.: dates de mises à jour)
- `graphs/` et `temp/` : contiennent des fichiers temporaires crées lors de l'exécution de certains scripts

### Fichiers

- `.gitignore` : contient les fichiers et dossiers générés par le logiciel et dont la présence sur le dépôt Git ne présente pas d'intérêt / est à éviter du fait de leur taille
- `.gitlab-ci.yml` : contient la configuration pour le déploiement des résultats visuels
- `Pipfile` & `Pipfile.lock` : contiennent la configuration de l'environnement Python virtuel
- `README.md` : est ce fichier / explicite
- `deploiement.ipynb` : est un Jupyter Notebook qui contient les visualisations pour le déploiement de la page Web
- `do_csv.py` : est un fichier Python qui exporte un fichier.csv contenant les données brutes et enrichies des DPE
- `run.py` : est un fichier Python qui orchestre l'exécution des différents fichiers présents dans le dossier `scripts/`