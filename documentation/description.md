# Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

L'exécution de `run.py` télécharge/met à jour les données souhaitées, si nécessaire, les transforme/nettoie, les charge en base de données.<br>
Le push de `deploiement.ipynb` exécute les programmes à but de visualisation et déploie le résultat en page Web.<br>

Le géocodage est refait à l'aide de adresse.data.gouv.fr.<br>
Le tri des données est fait à partir des résultats du géocodage, par géométrie.<br>