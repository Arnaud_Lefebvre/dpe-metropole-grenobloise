# documentation /

Le fichier `bogues_lacunes.md` : décrit les lacunes connues du logiciel  
Le fichier `conception.md` : parle des réflexions faites durant le projet  
Le fichier `description.md` : décrit le projet, en bref  
Le fichier `idees_amelioration.md` : décrit des pistes connues d'amélioration du projet  
Le fichier `journal.md` : décrit l'emploi du temps du développeur durant le projet  
Le fichier `notes.md` : est un brouillon de notes et devrait être supprimé d'ici la fin du projet  
Le fichier `references.md` : recense les références notables du projet  