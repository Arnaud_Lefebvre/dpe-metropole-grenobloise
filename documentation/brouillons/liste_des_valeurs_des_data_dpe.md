# Pour la colonne :id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <291> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :numero_dpe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0838L2000001F> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :usr_diagnostiqueur_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <151> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :usr_logiciel_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <14> est présente 441 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <7> est présente 786 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <9> est présente 1770 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <16> est présente 3571 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6> est présente 3670 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <11> est présente 4065 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <13> est présente 6270 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <12> est présente 13886 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2> est présente 14126 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <5> est présente 21313 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <10> est présente 39492 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 81579 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <5> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nom_methode_dpe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3cl> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6.2 - DPE> est présente 4 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <FACTURES> est présente 8 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-C-E - DPE> est présente 8 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-C-E> est présente 163 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <MIXTE FACTURE+3CL - DPE pour ECS> est présente 446 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <DPE VIERGE> est présente 801 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Facture> est présente 824 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <FACTURE SEULE> est présente 1622 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TH_BCE> est présente 1667 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <th-CE> est présente 1780 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :version_methode_dpe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6.B> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nom_methode_etude_thermique

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1er Decembre 2015> est présente 7 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1er déc 2015> est présente 60 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-C-E> est présente 163 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3CL - DPE> est présente 241 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <15 Août 2011> est présente 533 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-BCE> est présente 2138 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-CE 2005> est présente 4475 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <15 Août 2015> est présente 5392 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Th-BCE 2012> est présente 16831 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 161129 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :version_methode_etude_thermique

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2012> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :date_visite_diagnostiqueur

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2000-01-31> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :date_etablissement_dpe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1899-12-30> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :date_arrete_tarifs_energies

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2019-09-15> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :commentaires_ameliorations_recommandations

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <     > est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :explication_personnalisee

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <   Un écart est possible entre les consommations d'énergie issues de la simulation conventionnelle et celles réellement consommées.  En fonction du mode de vie et du nombre d'occupants les écarts peuvent être significatifs. > est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :consommation_energie

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <-120.1> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :classe_consommation_energie

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <G> est présente 6397 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <A> est présente 12382 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <F> est présente 15402 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <N> est présente 23992 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <B> est présente 26488 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <C> est présente 28148 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <E> est présente 34222 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <D> est présente 43938 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :estimation_ges

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.04> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :classe_estimation_ges

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <G> est présente 4652 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <F> est présente 9231 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <A> est présente 23907 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <N> est présente 23992 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <E> est présente 24517 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <B> est présente 24857 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <D> est présente 32998 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <C> est présente 46815 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 67883 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2> est présente 104320 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :secteur_activite

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Agricole> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr012_categorie_erp_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3.0> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4.0> est présente 4 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.0> est présente 5 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <5.0> est présente 23 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 3059 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187876 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :annee_construction

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_habitable

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.7> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_thermique_lot

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.7> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <38> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :commune

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <adastrale> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :arrondissement

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < CR> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :type_voie

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <  rue> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nom_rue

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < > est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :numero_rue

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < 					LE MALATRAS 					> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :batiment

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <  LES SOLAIRES R > est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :escalier

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <  A2   3° étage  porte de gauche> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :etage

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <-4> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :porte

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < Appartement n° 3> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :code_postal

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < 38260> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :code_insee_commune

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <	38544> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :code_insee_commune_actualise

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <	38544> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :numero_lot

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < - 122 Studio - 25 Casier à skis> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :quote_part

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0 / 0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nom_centre_commercial

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <CENTRE COMMERCIAL LECLERC> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_commerciale_contractuelle

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <100.0> est présente 90 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 20899 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 169980 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :portee_dpe_batiment

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.0> est présente 7817 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 15618 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 39492 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 128042 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :partie_batiment

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <B2> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :shon

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <11.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_utile

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <9.28> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_thermique_parties_communes

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <57.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :en_souterrain

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 43557 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 147410 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :en_surface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 12999 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 43557 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 134413 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nombre_niveaux

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <14.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nombre_circulations_verticales

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nombre_boutiques

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 43557 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 147412 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :presence_verriere

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 11 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 39492 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 151466 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_verriere

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 43557 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 147412 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :type_vitrage_verriere

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nombre_entrees_avec_sas

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :nombre_entrees_sans_sas

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_baies_orientees_nord

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.01> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_baies_orientees_est_ouest

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.07> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_baies_orientees_sud

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.02> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_planchers_hauts_deperditifs

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.01> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_planchers_bas_deperditifs

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.01> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :surface_parois_verticales_opaques_deperditives

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <-1.51> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :etat_avancement

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :organisme_certificateur

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < Bureau Veritas Certification> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :adresse_organisme_certificateur

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : < > est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :dpe_vierge

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 18963 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 21997 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 150009 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :est_efface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :date_reception_dpe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2013-04-12 04:00:00> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :longitude

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <-4.7788102> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :latitude

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <41.9369858> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_score

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.97> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.98> est présente 3 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.3> est présente 628 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.92> est présente 669 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.69> est présente 807 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.91> est présente 905 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.67> est présente 983 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.68> est présente 1182 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.32> est présente 1260 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.64> est présente 1265 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.6> est présente 1269 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_type

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <poi.1> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_adresse

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <"La Voie Romaine" (Chemin) Saint-Didier-en-Velay> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <01047_0060> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_l4

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1 A AVENUE DE LA RIDELET> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :geo_l5

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <BATIE DIVISIN> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <V5> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_type_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6> est présente 3148 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 36996 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4> est présente 50799 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 100026 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_modele

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6.3b> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_description

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Pour  les  bâtiments  à  occupation  continue : Consommation par Energie> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_fichier_vierge

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <5_V5_DPE_6.3b.pdf> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_est_efface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_type

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Copropriété> est présente 3148 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Neuf> est présente 36996 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Location> est présente 50799 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Vente> est présente 100026 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_type_libelle

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Copropriété> est présente 3148 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Neuf> est présente 36996 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Location> est présente 50799 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Vente> est présente 100026 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr001_modele_dpe_type_ordre

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6> est présente 3148 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 36996 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2> est présente 50799 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 100026 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR002_003> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR002_001> est présente 67883 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR002_002> est présente 104320 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_description

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Bâtiment collectif à usage principal d'habitation> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Maison Individuelle> est présente 67883 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Logement> est présente 104320 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_libelle

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Logements collectifs> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Maison> est présente 67883 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Appartement> est présente 104320 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_est_efface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_ordre

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 67883 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2> est présente 104320 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr002_type_batiment_simulateur

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0> est présente 18766 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 172203 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr012_categorie_erp_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR012_003> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR012_004> est présente 4 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR012_002> est présente 5 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR012_005> est présente 23 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR012_001> est présente 3059 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187876 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr012_categorie_erp_categorie

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3ème Catégorie> est présente 2 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4ème Catégorie> est présente 4 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2ème Catégorie> est présente 5 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <5ème Catégorie> est présente 23 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1ère Catégorie> est présente 3059 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187876 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr012_categorie_erp_groupe

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2ème Groupe> est présente 23 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1er Groupe> est présente 3070 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187876 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr012_categorie_erp_est_efface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 3093 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187876 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <TR013_002> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_type

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <GHO : Hôtel> est présente 1 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_categorie_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <2.0> est présente 13 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.0> est présente 122 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3.0> est présente 3446 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187388 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_est_efface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0.0> est présente 3581 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187388 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tr013_type_erp_categorie

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Établissements spéciaux> est présente 13 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Établissements installés dans un bâtiment> est présente 122 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Immeubles de grande hauteur (IGH)> est présente 3446 fois.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 187388 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <38> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_departement

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <38 - Isère> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_id

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_altmin

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_altmax

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6000> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_nref

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4800> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_dhref

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <55000> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_pref

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <100> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_c2

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <340.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_c3

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <1.5> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_c4

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <None> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_t_ext_basse

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <-10> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_e

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <480.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fch

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <26.1> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fecs_ancienne_m_i

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <54.5> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fecs_recente_m_i

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <68.9> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fecs_solaire_m_i

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <92.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fecs_ancienne_i_c

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <31.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv016_departement_fecs_recente_i_c

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <44.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <H1> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_t_ext_moyen

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6.58> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_peta_cw

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <10.5> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_dh14

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <42030> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv017_zone_hiver_prs1

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3.6> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <Ec> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_sclim_inf_150

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_sclim_sup_150

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <6> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_rclim_autres_etages

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <3.0> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

# Pour la colonne :tv018_zone_ete_rclim_dernier_etage

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La valeur : <4> est présente 190969 fois.

&nbsp;&nbsp;&nbsp;&nbsp;et 0 lignes avec des valeurs moins souvent présentes.
-----

