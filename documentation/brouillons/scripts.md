Le script Jupyter Notebook

Le script Python main.py appelle les autres.<br/>

Le script Python splitter_decorator est un decorator pour le rendu console lors de l'exécution des scripts.<br/>

Le script Python setup.py vérifié l'état du dossier du projet en préparation des étapes suivantes.<br/>
Le script Python download.py télécharge les jeux de données qui ne sont pas présents en local.<br/>
Le script Python load.py injecte les jeux de données bruts dans la base de données.<br/>
Le script Python duplicate.py duplique les tables de données brutes dans des tables à but d'exploitation / transformation.<br/>
Le script Python normalize_column_str.py transforme des colonnes de type `string` pour augmenter la cohérence des données.<br/>

Le script Python tune_levenshtein.py sert à tester la fonction `Levenshtein.ratio()` pour optimimser son efficacité.<br/>
Le script Python transform_iris_projections.py sert à transformer les coordonnées IRIS de type Lambert 93 en coordonnées classiques EPSG 4326 (lon, lat).<br/>
Le script Python count_null_values.py sert à compter les valeurs nulles à travers une table.<br/>
Le script Python list_dpe_column_values.py sert à compter les valeurs les plus souvent présentes dans une table.<br/>
Le script Python make_entire_metro_geojson.py sert à obtenir le contour de l'ensemble de la métropole de Grenoble à partir des données des communes.<br/>