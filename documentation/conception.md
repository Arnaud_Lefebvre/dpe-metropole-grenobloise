# Conception du projet

## Vision globale du projet

Le projet concerne les diagnostics de performance énergétique de l'intercommunalité Grenoble-Alpes-Auvergne que nous souhaitons analyser et visualiser.<br>
Il faudra donc prendre connaissance des données disponibles (indiquées par le client, fournies par l'ADEME), les travailler et extraire des indicateurs et des coordonnées géographiques.<br>
Il faudra présenter ces analyses publiquement (site Web) et les visualiser.<br>
Il faudra mettre en place un programme facilement réutilisable pour que le projet se déploie automatiquement.<br>
Il faudra documenter.<br>
Il faudra que la mise à jour des données puisse être faite automatiquement et que tout le processus de traitement de la donnée soit répercutée sur la mise à jour.

## Objectifs

- Filtrer les données iséroises pour obtenir celles de la Métropole.<br>
- Gérer les doublons dans le jeu de données des DPE.<br>
- Obtenir les données du nombre de logements par commune de la Métropole.<br>
- Obtenir les données du nombre de logements par maille IRIS de la Métropole.<br>
- Conserver les données à l'état brut.<br>
- Re-travailler les colonnes adresses en vue d'un géocodage.<br>
- Refaire le géocodage des adresses.<br>
- Placer les DPE dans les mailles IRIS par géométrie.<br>
- Mettre à jour le jeu de données via utilisation de l'API.<br>
- Faire des visualisations de type cartographie.<br>
- Faire des visualisations de type graphiques à indicateurs.<br>
- Autres / notes<br>

## Réalisation

- Chercher à savoir si plusieurs DPE existent pour un seul logement.

### Filtrer les données iséroises pour obtenir celles de la Métropole.

Au moins 3 pistes existent, filtrer avec le nom des communes, filtrer avec les codes INSEE, filtrer par géolocalisation.<br>
Aucune de ces trois données n'est parfaitement fiable, des valeurs erronées étant présentes à chaque fois.<br>

On sait que :<br>

- Les données code INSEE sont assez imprécises et semblent contenir de nombreuses valeurs fausses/de nombreux codes postaux<br>
- Les données communes sont probablement assez fiables, mais nécessitent un travail de nettoyage pour harmoniser (entre autres) les orthographes, laissées libres dans le jeu.<br>
- Les données géographiques sont manifestement d'assez mauvaise qualité, avec des points pour l'Isère situés dans la France entière.<br>

L'idéal serait de travailler chacune de ces colonnes pour augmenter la confiance des les données ainsi que pour réussir (potentiellement) à filtrer des données pour lesquelles une seule de ces valeurs serait disponible.<br>

Le choix a été fait d'utiliser les adresses complètes, incluant les code postal, code INSEE et nom de la commune à travers un géocodage et de filtrer les valeurs en fonction de leur position géographique obtenue.<br>
En effet il m'a semblé que les erreurs étaient moins probables sur des adresses et qu'ainsi, si le géocodage était d'assez qualité, ce serait le choix le plus fiable pour le tri des données.<br>
Cette méthode a été appliquée sur l'intégralité du jeu de données pour récupérer les DPE qui, potentiellement, appartiendraient à la Métropole mais n'y seraient pas enregistrés.<br>
Le travail sur le jeu de données de l'Isère entier ouvre également la possibilité de tirer des visualisations comparative entre la Métropole et l'Isère.<br>

Aussi :<br>
Après formattage des noms des communes :<br>
- Mise en minuscules de tous les caractères<br>
- Retrait de tous les accents<br>
- Harmonisation des abbréviations : `saint(e)` -> `st(e)`<br>
- Retrait de tous les caractères spéciaux (guillemets, apostrophes...)<br>
- Retrait de tous les chiffres<br>
- Retrait de tous les `cedex`<br>
- Retrait de tous les résidus (doubles espaces, espaces en début de ligne...)<br>
En utilisant le module Levenshtein, dans le cas de communes ne correspondant pas le ratio est systématiquement inférieur ou égal à 0.8571428571428571<br>
Dans le cas de communes correspondant le ratio est systématiquement supérieur ou égal à 0.8888888888888888<br>
Environ 450 cas distincts vérifiés manuellement.<br>
Appliquer un tri sur les noms des communes avec ce formattage et en ne conservant que les ratios supérieur à 0.88 devrait donc permettre d'approcher un tri parfait en fonction des noms des communes.<br>

> Etalab sur son site Web nous informe que les coordonnées du jeu de données sur les DPE ont été géocodées. (établies à partir des adresses)<br>
> Les adresses ne sont cependant pas/presque pas harmonisées, ni dans un état convenable pour être utilisées dans un processus de géocodage.<br>
> Il est nécessaire de travailler les différentes colonnes contenant des données d'adresse pour extraire et identifier les informations qu'elles contiennent.<br>
> Une fois les adresses dans un état assez propre, il devrait être possible de les géocoder et d'améliorer la qualité des coordonnées du jeu.<br>

> Les colonnes geo_*, longitude et latitude sont parfois entièrement vides malgré que des informations sont disponibles sur l'adresse.<br>
> Il sera donc possible, a minima, d'enrichir le jeu de données (partie ou tout des 25-30k coordonnées manquantes)<br>
> Environ 33k numéros de rue sont absents

### Gérer les doublons dans le jeu de données des DPE

Malgré que je n'ai pas pu prouver leur existence, il semble évident que des doublons doivent peupler le jeu de données des DPE.<br>
> En effet, rien n'interdit qu'un logement ait été diagnostiqué plusieurs fois. (au contraire !)<br>
Le jeu de données ne contient que des données de deux types :<br>
- Des valeurs uniques au DPE (mais pas uniques au logement)<br>
- Des valeurs partagées par de nombreux logements<br>
Il semble donc impossible de dédoublonner le jeu.<br>
> Un dédoublonnage *partiel* serait certainement possible puisque certaines adresses sont précises à la porte près, mais ne présenterait pas vraiment d'intérêt.<br>
Bien que le dédoublonnage permettrait une sélection plus grande de visualisations (e.g.: évolution moyenne des DPE pour un même logement), il ne semble présenter d'intérêt majeur que pour la définition du taux de couverture.<br>
En effet les DPE étant faits (normalement) à chaque changement de locataire, leur mesure pourrait s'exprimer, par exemple, en logements/année. (pour une quantité fixe de logements, x DPE seront produits chaque année)<br>
Il y a donc un décalage avec le nombre de logements dont l'unité serait simplement, pour le même exemple, logements.<br>

### Obtenir les données du nombre de logements par commune de la Métropole

Un jeu de données est accessible ici par exemple :<br>
https://www.insee.fr/fr/statistiques/4515532?sommaire=4516107#consulter<br>
Mais il est possible via ce lien :<br>
https://statistiques-locales.insee.fr/<br>
&nbsp;&nbsp;&nbsp;&nbsp;qui mène ici :<br>
&nbsp;&nbsp;&nbsp;&nbsp;https://statistiques-locales.insee.fr/#view=map1&c=indicator<br>
&nbsp;&nbsp;&nbsp;&nbsp;d'utiliser le site (d'utilisation similaire à une base de données) pour obtenir uniquement les données voulues, en format `.csv`, d'une taille d'environ 1ko seulement.<br>
C'est le choix fait pour raisons pratiques.<br>

### Obtenir les données du nombre de logements par maille IRIS de la Métropole

Je n'ai pas réussi à obtenir les données par maille IRIS de la même manière, j'ai donc utilisé le jeu de données pour la France entière, trié.<br>
https://www.insee.fr/fr/statistiques/4799305#consulter<br>

> Les données sur le nombre de logements (communes et IRIS) datent de 2017, ce qui n'est pas idéal. Il serait possible d'extrapoler à partir des nombres de logements des années précédentes, mais je ne l'ai pas fait. D'autres sources de données pourraient exister mais mes recherches n'ont pas été fructueuses.<br>

### Conserver les données à l'état brut.

Je fais le choix en considération des besoins et de mes compétences de conserver les données brutes en base de données.<br>
Les transformations nécessaires étant probablement nombreuses il semble préférable de les faire étape par étape et je crois que j'y arriverai mieux en requêtant les données brutes via une base de données.<br>

### Re-travailler les colonnes adresses en vue d'un géocodage.

24 cas de figures différents (dans `scripts/transform_store.py`) ont permis de dégrossir le travail et d'augmenter grandement la qualité du géocodage.<br>
De nombreux cas différents existent, il arrive même que deux adresses différentes soient présentes pour une seule ligne et le nettoyage pourra être amélioré pendant longtemps a priori.<br>
Il est nécessaire de faire attention lors du nettoyage pour ne pas "sur-nettoyer", ou supprimer des données autres que la cible du nettoyage.<br>
Il serait idéal d'arriver à récupérer les informations sur l'étage parfois présentes dans les colonnes d'adresse pour tirer des graphiques en fonction de ces étages.<br>
L'API adresse.data.gouv.fr inclut une fonctionnalité permettant d'envoyer le(s) code(s) postal et/ou INSEE des adresses qu'on géocode. Un travail préalable sur ces deux colonnes permettrait surement d'augmenter la qualité du géocodage.<br>
Les données sont réparties erratiquement entre les différentes colonnes d'adresse (e.g.: la colonne `numero_rue` peut très bien contenir l'adresse complète) Il est donc nécessaire de traiter toutes ces colonnes d'adresse.<br>
L'ordre semble cependant être toujours respecté, quelle que soit la répartition dans les différentes colonnes : `numero_rue`, `type_voie`, `nom_rue`, `code_postal` puis `commune`.<br>

### Refaire le géocodage des adresses.

L'API geo.api.gouv.fr/adresse m'a été conseillée et me semble efficace et fiable.<br>
Ce sera donc l'outil choisi pour ré-effectuer les géolocalisations à partir des adresses.<br>
Le requêtage ligne par ligne est limité à 7 par seconde et durerait trop longtemps.<br>
> Il pourrait cependant être bon de l'implémenter pour les mises à jour de petite taille.<br>
Le requêtage de masse (en envoyant un fichier.csv) est efficace, mais "casse" dans certains cas : une ligne échouant peut interrompre le reste du processus.<br>
De fait il est préférable de scinder en plusieurs fichiers.csv plus petits (pour limiter la perte en cas d'échec).<br>
Aussi cela rend crucial, de re-travailler les adresses jusqu'à éviter, a minima, ces cassures qui font perdre beaucoup de données.<br>
Aucun résultat parfait n'a été obtenu, mais les pertes ont à ce jour été limitées à quelques milliers, augmentant apparemment la qualité du géocodage.<br>

### Placer les DPE dans les mailles IRIS par géométrie.

De nombreuses façons existent pour manipuler les géométries.<br>
Je choisis d'utiliser le module `shapely` qui est simpliste et suffisant pour les besoins du projet. (ainsi pas besoin d'outil plus complexe)<br>
Une maille IRIS est entièrement contenue dans une autre.<br>
&nbsp;&nbsp;&nbsp;&nbsp;La maille IRIS plus grande est incomplète : le trou qu'elle contient (qui correspond à la plus petite maille IRIS) n'est pas défini dans les données IRIS de l'IGN.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Il est donc nécessaire de traiter cette exception pour éviter d'avoir soit des doublons, soit des points "volés" par la maille IRIS plus grande.

### Mettre à jour le jeu de données via utilisation de l'API

L'utilisation ou non d'une clé API (qu'on peut créer ici https://koumoul.com/s/data-fair/settings après inscription) semble ne rien changer en ce qui concerne ce projet.<br>
Des données sont simples à obtenir avec un téléchargement minime, entre autres :<br>
- La dernière date de mise à jour enregistrée
- La dernière date de réception du DPE du jeu de données
- Le compte du total de lignes dans le jeu de données
- Le compte du total de valeurs uniques par colonne dans le jeu de données

Donc mise en place de la récupération de ces informations pour la vérification d'occurence ou non d'une mise à jour.<br>

Il est par ailleurs possible de télécharger des lignes spécifiques dans le jeu de données, mais une limite est fixée à 10 000 lignes téléchargées.<br>
Il n'est pas possible à ma connaissance de demander de sauter des lignes dans la requête.<br>
Il est apparemment possible, cependant, de passer une requête `elasticsearch` via l'URL de requête.<br>

Après multiples essais infructueux, une option semble fonctionner (et a fonctionné lors des tests effectués) pour les cas de mises à jour supérieures en taille à 10 000 lignes :<br>
Il s'agit de passer un paramètre de filtrage par mois de `date_reception_dpe` ce qui permet de restreindre les résultats à moins de 10 000 lignes (avec une marge potentiellement suffisante : 4 000 lignes environ obtenues en moyenne, 6 000 lignes environ obtenues au maximum)<br>
Il est ainsi possible de requêter l'API mois par mois à partir du mois actuel et de remonter le temps jusqu'à avoir récupéré tous les résultats.<br>
> Certaines erreurs dans le jeu de données pourraient briser cette fonctionnalité : une date ultérieure à la date actuelle (i.e.: une date n'ayant pas encore eu cours) ne serait jamais traitée et la mise à jour ne pourrait pas être faite de cette façon<br>
Une fonctionnalité à été implémentée pour que le fichier.csv intégral soit téléchargé en cas de mise à jour et d'échec de tous les requête à l'API.<br>

### Faire des visualisations de type cartographie

Pour les raisons évoquées dans la rubrique `Gérer les doublons dans le jeu de données des DPE`, la représentation du taux de couverture a été retirée de la page Web déployée (parce qu'il est faux). Il serait peut-être possible d'extrapoler le nombre de logements/année à partir de durées moyennes de location, ou de dédoublonner à l'aide de données sur les baux locatifs et ainsi de réimplémenter ce taux de couverture.<br>
Une visualisation des moyennes des DPE a été faite.<br>
Une visualisation par marqueurs du travail de géocodage effectué a été faite.<br>
La cartographie avec marqueurs a posé des problèmes de performance. Mes recherches pour modifier la façon donc cette cartographie est chargée n'ont pas été fructueuses, ainsi je me suis rabattu sur les options :<br>
- Regroupement des points à même coordonnées, pour limiter la quantité totale de points.<br>
- Minimalisme : pas de "tooltip" ni de "popup" pour ces marqueurs, ces derniers influant grandement sur les performances.<br>

### Faire des visualisations de type graphiques à indicateurs

Visualisations des répartitions des DPE.<br>
Visualisation par année de construction.<br>
Visualisations sur le lien entre les DPE.<br>
> La visualisation de type scatterplot est la plus intéressante et met en valeur l'impact du choix du modèle d'estimations d'émissions de gaz à effet de serre.<br>
Visualisations par type de bâtiment.<br>
Visualisation par surface habitable.<br>

### Autres / notes

La question de l'anonymat s'est posée, mais les DPE ne semblent pas être des informations personnelles.<br>
Les `geojson` des mailles IRIS et des communes ont été comparés et sont cohérents entre eux.<br>
Pas de mauvaise données ici a priori.<br>
Les deux colonnes `code_insee_commune` et `code_insee_commune_actualise` sont identiques, à une unique ligne près.<br>
Le site :<br>
https://www.cap-logement-etudiant.com/residence-etudiante/auvergne-rhone-alpes/isere/grenoble/oxygene-m38365.htm<br>
est celui d'une résidence étudiante récemment construite, proposant 112 logements. 378 lignes existe pour cette même adresse dans le jeu de données, semblant attester de l'existence de doublons.<br>
Il y a des visites diagnostiqueur en date de 2150.<br>
Citation Etalab / data.gouv : "Etalab a donc mis en place un traitement pour le géocodage de la base de données. Le géocodage consiste à affecter des coordonnées géographiques à une adresse postale. Pour cela, Etalab a utilisé le géocodeur Addok en s'appuyant sur la Base d'Adresse Nationale (BAN)."<br>
source : https://www.data.gouv.fr/fr/posts/la-base-des-diagnostics-de-performance-energetique-dpe/.<br>