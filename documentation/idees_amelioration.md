# Idées d'améliorations

Extraire des statistiques précises des défauts des données et des nettoyages effectués.<br>

Faire un CI/CD pour vérification automatique de la mise à jour, avec déploiement en cas de changement des valeurs de mise à jour.<br>
Push `records/dpe_update_stamp` (qui conserve les informations de mise à jour) à cet effet.<br>

Trier les mailles IRIS par géométrie plutôt que par code INSEE ou nom de la commune. (plus fiable a priori)<br>

Chercher à visualiser les potentiels clusters de passoires thermiques.<br>

Récupérer un registre des codes INSEE pour regarder si les mauvais codes INSEE sont des codes INSEE "décalés" (e.g.: 38184 au lieu de 38185)<br>
Il est possible que ces erreurs de codes INSEE représentent le choix d'un ligne avant/après dans une liste, ou bien qu'elles soient une erreur sur un chiffre seulement.<br>
Il est possible que beaucoup de codes INSEE soient rattrapable de cette façon (mais je manque de connaissances à ce jour pour en juger).<br>

Croiser les valeurs des code INSEE, communes, adresses pour chercher en profondeur quels DPE sont de la Métropole, et lesquels n'en sont pas.<br>

Récupérer des données sur les baux locatifs (si elles existent/sont disponibles) pour dédoublonner le jeux des DPE, pour faire un taux de couverture correct.<br>
A ce jour, les doublons des DPE n'ont pas été gérés.<br>
Option : trouver le temps moyen qu'un résident français/grenoblois se loge jusqu'au déménagement.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Extrapoler pour obtenir le nombre de logements dans la Métropole /année, ce qui correspondrait à l'unité nécessaire pour faire un taux de couverture correct.<br>

Utiliser les emprises au sol des bâtiments (cadastre) pour présenter les points individuels plutôt par bâtiment, avec leur forme.<br>

Chercher à obtenir des données sur les bâtiments publics.<br>
Croiser ces données avec les emprises au sol pour extraire une cartographie des bâtiments publics et de leurs DPE.<br>

Cartographier les logements non-recensés (taux de couverture inversé).<br>

Faire une cartographie chronologique dynamique.<br>
> Nécessité peut-être de changer de méthode de déploiement pour intégrer le dynamisme.<br>

Faire une/des visualisation(s) par secteur d'activité.<br>
Faire une/des visualisation(s) pour l'impact de l'étage/la taille de l'immeuble sur les DPE.<br>
Explorer l'impact des modèles d'estimation par secteur d'activité, organisme diagnostiqueur, méthode calcul, date...<br>

Améliorer le code<br>
&nbsp;&nbsp;&nbsp;&nbsp;Construire des classes pour un code plus élégant. (e.g.: connection_open(), connection_close() --> class: Connection: def open(): def close():)<br>
&nbsp;&nbsp;&nbsp;&nbsp;Assigner les variables à leur scopes respectives plutôt que dans un fichier partagé `variables.py`<br>