# Utilisés

### Jeux de données de l'IGN dont le jeu avec les contours IRIS
https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#irisge

### Jeu de données de l'opendata de la métropole grenobloise listant les communes de l'intercommunalité Grenoble-Alpes-Auvergne
https://data.metropolegrenoble.fr/ckan/dataset/les-communes-de-la-metropole

### Doc API koumoul
https://koumoul-dev.github.io/data-fair/

### Jeu de données des diagnostics de performance énergétique des logements de France
https://data.ademe.fr/datasets/dpe-france
#### Jeu de données des diagnostics de performance énergétique des logements d'Isère
https://data.ademe.fr/datasets/dpe-38

### Cadastre opendata
https://cadastre.data.gouv.fr/datasets/cadastre-etalab
#### Cadastre isérois opendata
https://cadastre.data.gouv.fr/data/etalab-cadastre/2021-02-01/geojson/departements/38/

### Nombre de logements /!\ 2017
https://www.data.gouv.fr/fr/datasets/logement/
###### Vers
https://www.insee.fr/fr/statistiques
###### Vers
https://www.insee.fr/fr/information/3544265
###### Vers
https://statistiques-locales.insee.fr/#c=report&chapter=compar&report=r01&selgeo1=com_courant.38185
###### Intercommunalité Grenoble-Alpes-Métropole
https://statistiques-locales.insee.fr/#c=report&chapter=compar&report=r01&selgeo1=epci.200040715
###### Nombre de logements
https://statistiques-locales.insee.fr/#bbox=568645,5683677,136192,83167&c=indicator&i=bdcom.log&s=2017&view=map1
###### Nombre de logements par IRIS
https://www.insee.fr/fr/statistiques/4799305#consulter

### Inscription pour obtenir clef API
https://koumoul.com/en
https://koumoul.com/en/platform/reference-data
### Documentation API
https://koumoul.com/en/documentation/developer

### License opendata
https://api.gouv.fr/les-api/api_dpe_batiments_publics


# Exploratoires

### Infos DPE
https://www.data.gouv.fr/fr/posts/la-base-des-diagnostics-de-performance-energetique-dpe/
https://www.ecologie.gouv.fr/diagnostic-performance-energetique-dpe

### Opendata grenoblois
https://data.metropolegrenoble.fr/ckan/dataset?q=&sort=score+desc%2C+metadata_modified+desc

### Jeux de données et visualisations provenant du fournisseur?/hébergeur?/.. des données sur les DPE
https://opendata.koumoul.com/



# En attente de vérification

### Contours en format WGS84 (World Geodetic System 1984)(ou EPSG:4326)(système courant pour de la cartographie simple)
> En date de 2013 apparemment - A vérifier si suffisamment de temps pour économies process time et/ou hard drive/download...
https://www.data.gouv.fr/fr/datasets/contour-des-iris-insee-tout-en-un/

### Infos DPE
https://www.legifrance.gouv.fr/eli/arrete/2012/12/24/ETLL1242680A/jo
https://www.legifrance.gouv.fr/loda/id/JORFTEXT000025509925/
https://www.legifrance.gouv.fr/loda/id/JORFTEXT000025509969/

### DPE bâtiments tertiaires
https://data.ademe.fr/datasets/dpe-tertiaire



# Recherche d'informations (techniques)
https://stackoverflow.com/
https://gis.stackexchange.com/
https://plotly.com
https://www.kaggle.com/
https://www.python-graph-gallery.com/
https://github.com/
https://pypi.org/
https://python-visualization.github.io/
https://towardsdatascience.com/



# Non-utilisés

### Lien mort - données sur les bâtiments publics
https://www.data.gouv.fr/fr/datasets/diagnostics-de-performance-energetique-pour-les-batiments-publics/

### Lien mort - utilisation/documentation API
https://api.gouv.fr/documentation/api_dpe_batiments_publics

### Système d'information géographique
https://postgis.net/

### Recherche d'infos bâtiments publics
https://www.performance-publique.budget.gouv.fr/sites/performance_publique/files/files/documents/finances_publiques/revues_depenses/2016/RD2016_patrimoine_collectivites_territoriales.pdf



# Utilisés mais remplacés/retirés

### Partie des données cadastriales pour Grenoble dont coordonnées géographiques des emprises au sol des bâtiments
https://www.data.gouv.fr/fr/datasets/parcelles-et-batiments-1/