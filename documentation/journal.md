# Journal des activités principales exercées par demi-journées

### Résumé

###### (Approximatif)

Sur un total d'environ 30 jours:<br>
- Programmation/recherches mise à jour API<br>
7 jours<br>
- Programmation<br>
5 jours<br>
- Recherche technique<br>
8 jours<br>
- Recherche projet (enrichissement, exploration données...)<br>
7 jours<br>
- Gestion de projet<br>
3 jours

#### Vendredi 7 Mai 2021

Restitution du projet.

#### Jeudi 6 Mai 2021

##### Après-midi

Documentation.<br>
Finalisation programme.<br>
Visualisations.<br>

##### Matin

Documentation.<br>
Finalisation programme.<br>
Visualisations.<br>

#### Mercredi 5 Mai 2021

##### Après-midi

Documentation.<br>
Finalisation programme.<br>
Visualisations.<br>

##### Matin

Documentation.<br>
Finalisation programme.<br>
Visualisations.<br>

#### Mardi 4 Mai 2021

##### Après-midi

Finalisation programme.<br>
Visualisations.<br>

##### Matin

Finalisation programme.<br>
Visualisations.<br>

#### Lundi 3 Mai 2021

##### Après-midi

Finalisation programme.<br>
Visualisations.<br>

##### Matin

Finalisation programme.<br>
Gestion de projet.

#### Week-end

#### Vendredi 30 Avril 2021

##### Après-midi

Finalisation programme.<br>
Visualisations.<br>
Gestion de projet.

##### Matin

*Pas sur le projet*

#### Jeudi 29 Avril 2021

##### Après-midi

Finalisation programme.<br>
Gestion de projet.

##### Matin

Entretien Client.

#### Mercredi 28 Avril 2021

##### Après-midi

Ré-écriture programme pour le rendre mieux compréhensible et plus efficace<br>
Déboguage, vérifications en préparation du rendu final

##### Matin

Ré-écriture programme pour le rendre mieux compréhensible et plus efficace<br>
Déboguage, vérifications en préparation du rendu final

#### Mardi 27 Avril 2021

##### Après-midi

Ré-écriture programme pour le rendre mieux compréhensible et plus efficace<br>
Déboguage, vérifications en préparation du rendu final

##### Matin

Implémentation fonctionalité mise à jour via API<br/>
Ré-écriture programme pour le rendre mieux compréhensible et plus efficace

#### Lundi 26 Avril 2021

##### Après-midi

Implémentation fonctionalité mise à jour via API<br/>
Ré-écriture programme pour le rendre mieux compréhensible et plus efficace

##### Matin

Ré-écriture programme pour le rendre mieux compréhensible et plus efficace

#### Week-end

*Repos*<br/>
Ré-écriture programme pour le rendre mieux compréhensible et plus efficace

#### Vendredi 23 Avril 2021

*Congé*<br/>
*Repos*

#### Jeudi 22 Avril 2021

##### Après-midi

Succés utilisation elastic search API

##### Matin

Préparation entretien client<br/>
Entretien client<br/>
Visualisations

#### Mercredi 21 Avril 2021

##### Après-midi

Nettoyage adresses<br/>
Géocodage<br/>
Visualisations

##### Matin

Nettoyage adresses<br/>
Géocodage

#### Mardi 20 Avril 2021

##### Après-midi

Nettoyage adresses<br/>
Géocodage<br/>
Requêtage API gouv<br/>
Recherche informations techniques visualisations

##### Matin

Recherche informations elastic search pour API dpe<br/>
Recherche informations techniques visualisations

#### Lundi 19 Avril 2021

##### Après-midi

Recherche informations elastic search pour API dpe<br/>
Recherche informations techniques visualisations<br/>
Géocodage

##### Matin

*Pas sur le projet*

#### Week-end

*Repos*

#### Vendredi 16 Avril 2021

##### Après-midi

*Pas sur le projet*

##### Matin

*Pas sur le projet*

#### Jeudi 15 Avril 2021

##### Après-midi

*Pas sur le projet*

##### Matin

Entretien client<br/>
Rédaction documentation<br/>
Gestion de projet

#### Mercredi 14 Avril 2021

##### Après-midi

Recherches techniques visualisations<br/>
Visualisations<br/>
Déploiement

##### Matin

Recherches techniques visualisations<br/>
Visualisations

#### Mardi 13 Avril 2021

##### Après-midi

Traitement format données adresses<br/>
Recherches techniques

##### Matin

Traitement format données adresses<br/>
Recherches techniques

#### Lundi 12 Avril 2021

##### Après-midi

Utilisation API adresses gouv<br/>
Traitement format données adresses<br/>
Recherches techniques

##### Matin

Mise au propre code<br/>
Documentation<br/>
Gestion de projet<br/>
Recherches techniques

#### Week-end

*repos*<br/>
Mise au propre code

#### Vendredi 09 Avril 2021

##### Après-midi

Implémentation code utilisation API

##### Matin

Succés utilisation API pour mise à jour des données<br/>
Début de l'implémentation du code pour utilisation de l'API

#### Jeudi 08 Avril 2021

##### Après-midi

Exploration possibilités API

##### Matin

Prépartion point client<br/>
Point client

#### Mercredi 07 Avril 2021

##### Après-midi

Travail cartographie<br/>
Mise au propre code<br/>
Exploration enrichissement<br/>
Préparation point client

##### Matin

Exploration enrichissement<br/>
Travail cartographie<br/>
Préparation point client<br/>
*Travail hors-projet*

#### Mardi 06 Avril 2021

##### Après-midi

Mise au propre code<br/>
Exploration

##### Matin

Mise au propre code<br/>
Exploration

#### Lundi 05 Avril 2021

##### Après-midi

*Férié, repos*<br/>
Mise au propre code<br/>
Exploration enrichissement

##### Matin

*Férié, repos*<br/>
Mise au propre code<br/>
Exploration enrichissement

#### Week-end

*Repos*<br/>
Mise au propre du code

#### Vendredi 02 Avril 2021

##### Après-midi

Exploration possibilités

##### Matin

*Pas sur le projet*

#### Jeudi 01 Avril 2021

##### Après-midi

Exploration des données<br/>
Exploration des possibilités en cartographie<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Confirmation possibilité de localiser des points dans des régions<br/>
Exploration de l'API

##### Matin

*Pas sur le projet*

#### Mercredi 31 Mars 2021

##### Après-midi

Obtention lien vers INSEE pour le nobmre total de logements, plus qu'à implémenter le code<br/>
Exploration des données

##### Matin

Entretien avec la Métro et la Turbine<br/>
Création d'un Trello pour présentations futures<br/>
Exploration des données

#### Mardi 30 Mars 2021

##### Après-midi

Exploration code possibilités<br/>
Préparation maquettes visualisations<br/>
Préparation premier entretien individuel avec la Métro

##### Matin

Préparation maquettes visualisations

#### Lundi 29 Mars 2021

##### Après-midi

Expérimentations code, API Ademe

##### Matin

Entretien Turbine<br/>
Organisation Projet

#### Week-end

*Repos surtout - c'est le week-end%*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;*Réflexion*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;*Finalisation questions*

#### Vendredi 26 Mars 2021

##### Après-midi

Exploration projet<br/>
Préparation des questions

##### Matin

*Pas sur le projet*

#### Jeudi 25 Mars 2021

##### Après-midi

Exploration projet

##### Matin

Exploration projet<br/>
Schéma pipeline

# Début du projet

#### Mercredi 24 Mars 2021

##### Après-midi

###### 17:00 +

Découverte du projet