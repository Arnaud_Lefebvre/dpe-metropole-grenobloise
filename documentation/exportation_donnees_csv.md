# Sur l'exportation du jeu de données enrichi en format csv

Lors de l'utilisation du fichier do_csv.py présent dans la racine du dépôt GitLab, le fichier.csv créé contient la totalité des données brutes, ainsi que :

- des données enrichies et/ou transformées, qualifiées par le préfixe : `re_`, à savoir :
    - `re_code_insee_commune` : le code INSEE retravaillé
    - `re_code_postal` : le code postal retravaillé
    - `re_code_iris` : le code IRIS pour la géolocalisation refaite
    - `re_code_iris_raw_coords` : le code IRIS pour la géolocalisation initiale
    - `re_numero_rue` : le numéro de rue retravaillé (à des fins de géolocalisation)
    - `re_type_voie` : le type de voie retravaillé (à des fins de géolocalisation)
    - `re_nom_rue` : le nom de la rue retrvaillé (à des fins de géolocalisation)
    - `re_commune_normalized` : le nom de la commune retrvaillé (à des fins de géolocalisation et d'harmonisation des noms de commune à travers les différentes tables)
- des données enrichies et/ou transformées obtenues par le géocodage, qualifiées par le préfixe : `_re_geo_`, à savoir :
    - `re_geo_longitude` : la longitude obtenue par géolocalisation
    - `re_geo_latitude` : la latitude obtenue par géolocalisation
    - `re_geo_score` : le score de confiance (estimation de la probablité que le résultat obtenu soir correct) obtenu par géolocalisation
    - `re_geo_numero_rue` : le numéro de rue obtenu par géolocalisation
    - `re_geo_type_voie` : le type de voie obtenu par géolocalisation
    - `re_geo_nom_rue` : le nom de rue obtenu par géolocalisation
    - `re_geo_code_postal` : le code postal obtenu par géolocalisation
    - `re_geo_commune` : le nom de commune obtenu par géolocalisation
    - `re_geo_code_insee_commune` : le code INSEE obtenu par géolocalisation
    - `re_geo_context` : le *contexte* (département, région) obtenu par géolocalisation