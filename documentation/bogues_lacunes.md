# Listes des bogues et/ou lacunes connues du projet

Aucun test (/TDD) n'a été fait.<br>

La transformation des données peut être compromise si le script est interrompu :<br>
&nbsp;&nbsp;&nbsp;&nbsp;en effet, le logiciel charge les données **puis** les transforme, sans transaction.
&nbsp;&nbsp;&nbsp;&nbsp;De fait, si le logiciel est interrompu entre ces deux étapes, la transformation ne se déclenche pas.

Lors de la mise à jour via API, si une ligne était modifiée le logiciel ne vérifiant que les `id` et le nombre de lignes ne le prendrait pas en compte.

Le géocodage ligne par ligne est plus solide et il pourrait être préférable de l'implémenter pour les mises à jour.<br>
Cependant, l'API geo.api.gouv.fr/adresse limite à 7 requêtes par seconde.<br>
En cas d'échec sur une ligne lors du géocodage de masse sur cette même API (via fichier.csv), selon le cas, le reste de la requête peut échouer, perdant ainsi des données.<br>

Le nettoyage des données est très incomplet.<br>