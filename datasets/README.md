# datasets/

`coordonnees_iris_transformees.csv` : contient les coordonnees IRIS transformées en wgs84 (données originales en LAMBERT93)  
`geocoding_results.csv` : contient les données obtenues via l'API adresse.data.gouv.fr  
`nb_logements_par_commune.csv` : contient les données sur le nombre de logements par commune, obtenu manuellement depuis le site statistiques-locales.insee.fr  